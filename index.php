<?php

require './pages/core.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION["username"])) {
    header("Location: $home/pages/login.php");
} else {
    header("Location: $home/pages");
}

