<body>
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
            <div class="page-logo"><a class="navbar-brand" href="#">Child Friendly School</a></div>
            <div class="top_menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                            <span class="glyphicon glyphicon-user">&nbsp;</span>Signed in as <?php echo $_SESSION["username"]; ?> &nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="pages/profile.php"><span class="glyphicon glyphicon-plus">&nbsp;</span>View Profile</a></li>
                            <li role="presentation" class="divider"></li>
                            <li><a href="pages/logout.php"><span class="glyphicon glyphicon-log-out">&nbsp;</span>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid main-container">
        <div class="navigation col-sm-2" style="background-color: ">
            <ul class="nav main-menu">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#" id="administrator-group"><i class="fa fa-users"></i> Administrator<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="nav nav-pills nav-stacked col-sm-offset-1" id="administrator" style="">
                        <li><a  href="pages/user.php"><i class="fa fa-user"></i>Users</a></li>
                        <li><a  href="pages/user_registration.php"><i class="fa fa-group"></i>New User</a></li>
                    </ul>
                </li>	
                <li><a id="school-menu" href="pages/school.php"><i class="fa fa-home"></i> School</a></li>
                <li><a id="plan-menu" href="#"><i class="fa fa-flag-checkered"></i> Plan</a></li>
                <li><a id="school-plan-menu" href="#"><i class="fa fa-level-up"></i> School Plan</a></li>
                <li><a id="school-visit-menu" href="#"><i class="fa fa-level-up"></i> School Visit</a></li>
                <li><a id="school-visit-menu" href="#"><i class="fa fa-level-up"></i>Report</a></li>
            </ul>
        </div>
    </div>
</body>
</html>