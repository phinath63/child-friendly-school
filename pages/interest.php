<?php
$base = 45000.00;
$time = 240;
$rate = 0.007;

$monthly_pay = (float) $base / $time;
?>
<table border="1" style="border: solid 1px black;">
    <thead>
        <th>Month</th><th>Monthly Pay</th><th>Base</th>
        <th>Rate</th><th>Rate Pay</th><th>Pay</th>
    </thead>
<tbody>
    <?php
    $initial = $base;
    $rate_pay = 0;
    $rate_total = 0;
    $money_paid = 0;
    $total = 0;
    for ($i = 0; $i < $time; $i++) {
        echo "<tr>";
        echo "<td>" . ($i + 1) . "</td>";
        echo "<td>" . ($monthly_pay) . "</td>";
        $money_remain = $initial - $money_paid;
        $money_paid +=$monthly_pay;
        echo "<td>" . ($money_remain) . "</td>";
        echo "<td>" . ($rate * 100) . "%</td>";
        echo "<td>" . ($money_remain * $rate) . "</td>";
        echo "<td>" . ($monthly_pay + $money_remain * $rate) . "</td>";
        $total+=$monthly_pay + $money_remain * $rate;
        echo "</tr>";
    }
    echo "Total Payment:" . $total;
    ?> 
</tbody>
</table>

