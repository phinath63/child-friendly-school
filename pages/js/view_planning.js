var organization;
var province_code;
var district_code;


var showSelectProvince = function () {
    var def_pro = $("#default-province").html();
    var result = JSON.parse(getData('province', ['province_code', 'province_name']));
    for (i = 0; i < result.length; i++) {
        $("#select-province").append("<option value='" + result[i].province_code + "'" + (result[i].province_code === def_pro ? "selected" : "") + ">" + result[i].province_code + "-" + result[i].province_name + "</option>");
    }
};

var showSelectDistrict = function () {
    var def_distr = $("#default-district").html();
    $.ajax({
        url: 'page_proccessing.php',
        type: 'POST',
        data: {page: 'planning', action: 'select-district'},
        async: false,
        success: function (data, textStatus, jqXHR) {
            var str;
            data = JSON.parse(data);
            $("#select-district-code").html('');
            for (i = 0; i < data.length; i++) {
                str = "<option value='" + data[i].district_code + "'" + (data[i].district_code === def_distr ? "selected" : "") + ">" + data[i].district_code + "-" + data[i].district_name + "</option>";
                $("#select-district-code").append(str);
            }
        },
        error: function (jqXHR) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Unable to load district!');
            }
        }
    });
};
var fillDistrict = function () {
    var province_code = $("#select-province").val();
    var data = JSON.parse(getData('district', ['district_code', 'district_name'], 'province_code=' + province_code));
    $("#select-district-code").html('');
    for (i = 0; i < data.length; i++) {
        var str = "<option value='" + data[i].district_code + "'>" + data[i].district_code + "-" + data[i].district_name + "</option>";
        $("#select-district-code").append(str);
    }
};


var loadData = function (action) {
    var geography;
    geography = 'district';
    if (organization === 'PED' || organization === 'SED') {
        geography = $("#select-geography").val();
    }
    $("#loading").show();
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        async: false,
        data: {page: "planning", action: action, geography: geography},
        success: function (result) {
            // Redirect to home page
            var data = JSON.parse(result);
            var i;
            $("#data-list").html("");
            for (i = 0; i < data.length; i++) {
                var string;
                string = "<tr>" +
                        "<td class='" + plan_id_td + "'><span class='" + plan_id_span + "'>" + data[i].plan_id + "</span></td>" +
                        "<td class='" + plan_name_khmer_td + "'><span class='" + plan_name_khmer_span + "'>" + data[i].plan_name_khmer + "</span></td>" +
                        "<td class='" + plan_name_latin_td + "'><span class='" + plan_name_latin_span + "'>" + data[i].plan_name_latin + "</span></td>" +
                        "<td class='" + plan_commune_name_td + "'><span class='" + plan_commune_name_span + "'>" + data[i].commune_name + "</span></td>" +
                        "<td class='" + plan_village_name_td + "'><span class='" + plan_village_name_span + "'>" + data[i].village_name + "</span></td>" +
                        "<td class='" + plan_distance_to_DoE_in_km_td + "'><span class='" + plan_distance_to_DoE_in_km_span + "'>" + (data[i].distance_to_DoE_in_km === '0' ? '' : data[i].distance_to_DoE_in_km) + "</span></td>" +
                        "<td class='" + plan_distance_to_DoE_in_mn_td + "'><span class='" + plan_distance_to_DoE_in_mn_span + "'>" + (data[i].distance_to_DoE_in_minute === '0' ? '' : data[i].distance_to_DoE_in_minute) + "</span></td>" +
                        "<td class='" + plan_road_quality_td + "'><span class='" + plan_road_quality_span + "'>" + data[i].road_quality + "</span></td>" +
                        "<td class='" + plan_when_accessible_by_road_td + "'><span class='" + plan_when_accessible_by_road_span + "'>" + data[i].when_accessible_by_road + "</span></td>" +
                        (right_edit === '1' ? '<td>' + default_option + '</td>' : '') +
                        "</tr>";
                $("#data-list").append(string);
            }
            $("#loading").hide();
        },
        error: function (jqXHR) {
            $("#loading").hide();
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Unable to load data!');
            }
        }
    });
};

$(document).ready(function () {
    organization = $("#organization").html();
    right_edit = $("#right-to-edit-data").html();
    setHeight();
});

