var full_name;
var user_id;
var password;
var organization;
var province;
var district;
var dtmt_member;
var dtmt_defult_district;
var created_by;
var right_to_enter_data;
var right_to_edit_data;
var right_to_access_statistic;
var right_to_enter_school_distance;
var right_to_create_new_user;
var right_to_edit_user_you_created;

var full_name_td = "full-name-td";
var user_id_td = "user-id-td";
var organization_td = "organization-td";
var province_td = "province-td";
var district_td = "district-td";
var dtmt_member_td = "dtmt-member-td";
var dtmt_defult_district_td = "dtmt-default-district-td";
var created_by_td = "created-by-td";
var right_to_enter_data_td = "right-to-enter-data-td";
var right_to_edit_data_td = "right-to-edit-data-td";
var right_to_access_statistic_td = "right-to-access-statistic-td";
var right_to_enter_school_distance_td = "right-to-enter-distance-td";
var right_to_create_new_user_td = "right-to-create-new-user-td";
var right_to_edit_user_you_created_td = "right-to-edit-user-td";

var full_name_span = "full-name-span";
var user_id_span = "user-id-span";
var organization_span = "organization-span";
var province_span = "province-span";
var district_span = "district-span";
var dtmt_member_span = "dtmt-member-span";
var dtmt_defult_district_span = "dtmt-default-district-span";
var created_by_span = "created-by-span";
var right_to_enter_data_span = "right-to-enter-data-span";
var right_to_edit_data_span = "right-to-edit-data-span";
var right_to_access_statistic_span = "right-to-access-statistic-span";
var right_to_enter_school_distance_span = "right-to-enter-distance-span";
var right_to_create_new_user_span = "right-to-create-new-user-span";
var right_to_edit_user_you_created_span = "right-to-edit-user-span";

var default_province_list = [];
var district_list = [];
var organization_list = [{id: 'PED', des: 'PED'}, {id: 'SED', des: 'SED'}, {id: 'PoE', des: 'PoE'}, {id: 'DoE', des: 'DoE'}];
var yes_no = [{id: '0', des: 'No'}, {id: '1', des: 'Yes'}];
var row_id;
var right_edit;
var default_option = "<a data-toggle='modal' data-target='#edit' class='edit btn btn-sm btn-primary col-sm-10 col-sm-offset-1' onclick='edit(this)'>edit</a>";
function loadData(action) {
    $("#loading").show();
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        async: false,
        data: {page: "user", action: action},
        success: function (result) {
            // Redirect to home page
            var data = JSON.parse(result);
            var i;
            $("#data-list").html();
            for (i = 0; i < data.length; i++) {
                var string;
                string = "<tr>" +
                        "<td class='" + full_name_td + "'><span class='" + full_name_span + "'>" + data[i].full_name + "</span></td>" +
                        "<td class='" + user_id_td + "'><span class='" + user_id_span + "'>" + data[i].user_id + "</td >" +
                        "<td class='" + organization_td + "'><span class='" + organization_span + "'>" + data[i].organization + "</span></td>" +
                        "<td class='" + province_td + "'><span class='" + province_span + "'>" + (data[i].province === '0' ? '' : data[i].province) + "</span></td>" +
                        "<td class='" + district_td + "'><span class='" + district_span + "'>" + (data[i].district === '0' ? '' : data[i].district) + "</span></td>" +
                        "<td class='" + dtmt_member_td + "'><span class='" + dtmt_member_span + "'>" + (data[i].DTMT_member === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + dtmt_defult_district_td + "'><span class='" + dtmt_defult_district_span + "'>" + (data[i].DTMT_member === '0' ? '' : data[i].DTMT_defult_district) + "</span></td>" +
                        "<td class='" + created_by_td + "'><span class='" + created_by_span + "'>" + data[i].created_by + "</span></td>" +
                        "<td class='" + right_to_enter_data_td + "'><span class='" + right_to_enter_data_span + "'>" + (data[i].right_to_enter_data === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + right_to_edit_data_td + "'><span class='" + right_to_edit_data_span + "'>" + (data[i].right_to_edit_data === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + right_to_access_statistic_td + "'><span class='" + right_to_access_statistic_span + "'>" + (data[i].right_to_access_statistic === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + right_to_enter_school_distance_td + "'><span class='" + right_to_enter_school_distance_span + "'>" + (data[i].right_to_enter_school_distance === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + right_to_create_new_user_td + "'><span class='" + right_to_create_new_user_span + "'>" + (data[i].right_to_create_new_user === '0' ? 'No' : 'Yes') + "</span></td>" +
                        "<td class='" + right_to_edit_user_you_created_td + "'><span class='" + right_to_edit_user_you_created_span + "'>" + (data[i].right_to_edit_user_you_created === '0' ? 'No' : 'Yes') + "</span></td>" +
                        (right_edit === '1' ? '<td>' + default_option + '</td>' : '') +
                        "</tr>";
                $("#data-list").append(string);
            }
            $("#loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#loading").hide();
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Unable to load user!');
            }
        }
    });
}

var edit = function (row) {
    row_id = $(row).parent().parent();
    user_id = $(row_id).children("." + user_id_td).children("." + user_id_span).html();
    var user = JSON.parse(getData('user', ['*'], "user_id='" + user_id + "'"));
    full_name = user[0].full_name;
    user_id = user[0].user_id;
    password = user[0].password;
    organization = user[0].organization;
    province = user[0].province;
    district = user[0].district;
    dtmt_member = user[0].DTMT_member;
    dtmt_defult_district = user[0].DTMT_defult_district;
    created_by = user[0].created_by;
    right_to_enter_data = user[0].right_to_enter_data;
    right_to_edit_data = user[0].right_to_edit_data;
    right_to_access_statistic = user[0].right_to_access_statistic;
    right_to_enter_school_distance = user[0].right_to_enter_school_distance;
    right_to_create_new_user = user[0].right_to_create_new_user;
    right_to_edit_user_you_created = user[0].right_to_edit_user_you_created;

    $("#txt-full-name").val(full_name);
    $("#txt-user-id").val(user_id);
    $("#txt-password").val(password);
    $("#txt-con-password").val(password);
    $("#select-organization").html('');
    for (i = 0; i < organization_list.length; i++) {
        $("#select-organization").append("<option value='" + organization_list[i].id + "'" + (organization_list[i].id === organization ? "selected" : "") + ">" + organization_list[i].des + "</option>");
    }
    checkOrganization();
    if (organization === 'PoE' || organization === 'DoE') { // it has province and district
        $("#select-province").html('');
        for (i = 0; i < default_province_list.length; i++) {
            $("#select-province").append("<option value='" + default_province_list[i].province_code + "'" + (default_province_list[i].province_code === province ? "selected" : "") + ">" + default_province_list[i].province_code + "-" + default_province_list[i].province_name + "</option>");
        }
    }
    if (organization === 'DoE') { // it has province and district
        $("#select-district").html('');
        district_list = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + province));
        for (i = 0; i < district_list.length; i++) {
            $("#select-district").append("<option value='" + district_list[i].district_code + "'" + (district_list[i].district_code === district ? "selected" : "") + ">" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
        }
    }
    $("#select-dtmt-member").html('');
    for (i = 0; i < yes_no.length; i++) {
        $("#select-dtmt-member").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === dtmt_member ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    isDTMT();
    $("#select-right-to-enter-data").html('');
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-enter-data").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_enter_data ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    $('#select-right-to-edit-data').html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-edit-data").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_edit_data ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    $("#select-right-to-access-statistic").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-access-statistic").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_access_statistic ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    $("#select-right-to-enter-school-distance").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-enter-school-distance").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_enter_school_distance ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    $("#select-right-to-create-new-user").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-create-new-user").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_create_new_user ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
    $("#select-right-to-edit-user").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-right-to-edit-user").append("<option value='" + yes_no[i].id + "'" + (yes_no[i].id === right_to_edit_user_you_created ? "selected" : "") + ">" + yes_no[i].des + "</option>");
    }
};

var fillLocation = function (location) {
    if (location === 'province') {
        $("#select-province").html("");
        for (i = 0; i < default_province_list.length; i++) {
            $("#select-province").append("<option value='" + default_province_list[i].province_code + "'>" + default_province_list[i].province_code + "-" + default_province_list[i].province_name + "</option>");
        }
    }
    else if (location === 'district' && $("#select-organization").val() === 'DoE') {
        var province_code = $("#select-province").val();
        district_list = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + province_code));
        $("#select-district").html("");
        for (i = 0; i < district_list.length; i++) {
            $("#select-district").append("<option value='" + district_list[i].district_code + "'>" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
        }
    }
    fillDTMTType();
};

var isDTMT = function () {
    if ($("#select-dtmt-member").val() === '1') {
        $("#dtmt-type").show();
        fillDTMTType();
    }
    else {
        $("#dtmt-type").hide();
    }
};

var fillDTMTType = function () {
    if ($("#select-dtmt-member").val() === '1') {// find out if it is PED, SED, or PoE
        var organization = $("#select-organization").val();
        if (organization === 'PED' || organization === 'SED') {// list all the district
            district_list = JSON.parse(getData("district", ["district_code", "district_name"]));
            $("#select-dtmt-district").html("");
            for (i = 0; i < district_list.length; i++) {
                $("#select-dtmt-district").append("<option value='" + district_list[i].district_code + "'" + (district_list[i].district_code === dtmt_defult_district ? "selected" : "") + ">" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
            }
        }
        else if (organization === 'PoE') {// list all the district
            var province_code = $("#select-province").val();
            district_list = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + province_code));
            $("#select-dtmt-district").html("");
            for (i = 0; i < district_list.length; i++) {
                $("#select-dtmt-district").append("<option value='" + district_list[i].district_code + "'" + (district_list[i].district_code === dtmt_defult_district ? "selected" : "") + ">" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
            }
        }
        else if (organization === 'DoE') {
            $("#select-dtmt-district").html("");
            var district_code = $("#select-district").val();
            $("#select-dtmt-district").append("<option value='" + district_code + "'>" + district_code + "</option>");
        }
    }
};

var checkOrganization = function () {
    var organization = $("#select-organization").val();
    if (organization === 'PED' || organization === 'SED') {// hide province and istrict
        $("#province").hide(500);
        $("#district").hide(500);
    }
    else if (organization === 'PoE') {// hide district
        $("#province").show(500);
        $("#district").hide(500);
    }
    else {
        $("#province").show(500);
        $("#district").show(500);
        fillLocation('district');
    }
    fillDTMTType();
};

var checkField = function () {
    /*
     * 1 check for empty
     * 2 check for valid user id
     * 3 check for matched password
     */
    var boolean;
    boolean = true;
    var fullName = $("#txt-full-name").val();
    var user_id = $("#txt-user-id").val();
    var password = $("#txt-password").val();
    var conPassword = $("#txt-con-password").val();
    if (fullName.length === 0) {
        $("#lb-full-name").html("*Enter Full Name");
        boolean = false;
    }
    else {
        $("#lb-full-name").html("");
    }
    if (user_id.length < 3) {
        $("#lb-user-id").html("*User Id must be at least 3 characters");
        boolean = false;
    }
    else {
        $("#lb-user-id").html("");
    }
    if (password.length < 6) {
        $("#lb-password").html("*Password length must be at least 6 characters");
        boolean = false;
    }
    else {
        if (password !== conPassword) {
            $("#lb-password").html("*Password is not matched");
            boolean = false;
        }
        else {
            $("#lb-password").html("");
        }
    }
    return boolean;
};

var update = function () {
    if (checkField(checkField)) {
        var data = [];
        full_name = $("#txt-full-name").val();
        password = $("#txt-password").val();
        organization = $("#select-organization").val();
        province = $("#select-province").val();
        district = $("#select-district").val();
        dtmt_member = $("#select-dtmt-member").val();
        dtmt_defult_district = $("#select-dtmt-district").val();
        right_to_enter_data = $("#select-right-to-enter-data").val();
        right_to_edit_data = $("#select-right-to-edit-data").val();
        right_to_access_statistic = $("#select-right-to-access-statistic").val();
        right_to_enter_school_distance = $("#select-right-to-enter-school-distance").val();
        right_to_create_new_user = $("#select-right-to-create-new-user").val();
        right_to_edit_user_you_created = $("#select-right-to-edit-user").val();
        data = [full_name, password, organization, province, district,
            dtmt_member, dtmt_defult_district, right_to_enter_data, right_to_edit_data,
            right_to_access_statistic, right_to_enter_school_distance,
            right_to_create_new_user, right_to_edit_user_you_created, user_id];
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            data: {page: "user", action: "update", data: data},
            async: false,
            success: function (result) {
                var user = JSON.parse(result);
                $(row_id).children("." + full_name_td).children("." + full_name_span).html(user[0].full_name);
                $(row_id).children("." + user_id_td).children("." + user_id_span).html(user[0].user_id);
                $(row_id).children("." + organization_td).children("." + organization_span).html(user[0].organization);
                $(row_id).children("." + province_td).children("." + province_span).html((user[0].province === '0' ? '' : user[0].province));
                $(row_id).children("." + district_td).children("." + district_span).html((user[0].district === '0' ? '' : user[0].district));
                $(row_id).children("." + dtmt_member_td).children("." + dtmt_member_span).html(user[0].DTMT_member === '0' ? 'No' : 'Yes');
                $(row_id).children("." + dtmt_defult_district_td).children("." + dtmt_defult_district_span).html((user[0].DTMT_defult_district === '0' ? '' : user[0].DTMT_defult_district));
                $(row_id).children("." + right_to_enter_data_td).children("." + right_to_enter_data_span).html(user[0].right_to_enter_data === '0' ? 'No' : 'Yes');
                $(row_id).children("." + right_to_edit_data_td).children("." + right_to_edit_data_span).html(user[0].right_to_edit_data === '0' ? 'No' : 'Yes');
                $(row_id).children("." + right_to_access_statistic_td).children("." + right_to_access_statistic_span).html(user[0].right_to_access_statistic === '0' ? 'No' : 'Yes');
                $(row_id).children("." + right_to_enter_school_distance_td).children("." + right_to_enter_school_distance_span).html(user[0].right_to_enter_school_distance === '0' ? 'No' : 'Yes');
                $(row_id).children("." + right_to_create_new_user_td).children("." + right_to_create_new_user_span).html(user[0].right_to_create_new_user === '0' ? 'No' : 'Yes');
                $(row_id).children("." + right_to_edit_user_you_created_td).children("." + right_to_edit_user_you_created_span).html(user[0].right_to_edit_user_you_created === '0' ? 'No' : 'Yes');
                $('#edit').modal('toggle');
                alertMessage('success', 'User has been updated successfully!');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    $('#timeout').modal('toggle');
                }
                else {
                    alertMessage('fail', jqXHR.responseText);
                }
            }
        });
    }
};
$(document).ready(function () {
    right_edit = $("#right-to-edit").html();
    default_province_list = JSON.parse(getData("province", ["province_code", "province_name"]));
    fillLocation('province');
    setHeight();
    loadData('view');

});