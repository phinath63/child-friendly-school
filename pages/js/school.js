/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var right_edit;
var default_option = "<a data-toggle='modal' data-target='#edit' class='edit btn btn-sm btn-primary col-sm-10 col-sm-offset-1' onclick='edit(this)'>edit</a>";
var school_type = [{type: 'Close'}, {type: 'Medium'}, {type: 'Remote'}];
var yes_no = [{id: 0, name: 'No'}, {id: 1, name: 'Yes'}];
var road_type = [{type: 'Dirt'}, {type: 'Pave'}, {type: 'Only Moto'}];
var when_access = [{time: 'All Year'},
    {time: 'Road cut every year during rain'},
    {time: 'Road cut some year during rain'},
    {time: 'Road cut very rarely during rain'},
    {time: 'No Access by road'}];
var row_id;
// location
var province = [];
var default_district = [];
var default_commune = [];
var default_village = [];
var district = [];
var commune = [];
var village = [];
var school_code,
        school_name_khmer,
        school_name_latin,
        school_province,
        school_district,
        school_commune_code,
        school_commune_name,
        school_village_code,
        school_village_name,
        school_geolocation_x,
        school_geolocation_y,
        school_has_primary,
        school_has_lower_secondary,
        school_has_upper_secondary,
        school_has_vocational_training,
        school_last_visited_by_DTMT,
        school_distance_to_DoE_in_km,
        school_distance_to_DoE_in_mn,
        school_type_school,
        school_road_quality,
        school_when_accessible_by_road;
//
var organization = '';
var school_code_td = "school_code_td";
var school_name_khmer_td = "school_name_khmer_td";
var school_name_latin_td = "school_name_latin_td";
var school_province_td = "school_province_td";
var school_district_td = "school_district_td";
var school_commune_code_td = "school_commune_code_td";
var school_commune_name_td = "school_commune_name_td";
var school_village_code_td = "school_village_code_td";
var school_village_name_td = "school_village_name_td";
var school_geolocation_x_td = "school_geolocation_x_td";
var school_geolocation_y_td = "school_geolocation_y_td";
var school_has_primary_td = "school_has_primary_td";
var school_has_lower_secondary_td = "school_has_lower_secondary_td";
var school_has_upper_secondary_td = "school_has_upper_secondary_td";
var school_has_vocational_training_td = "school_has_vocational_training_td";
var school_last_visited_by_DTMT_td = "school_last_visted_by_DTMT_td";
var school_distance_to_DoE_in_km_td = "school_distance_to_DoE_td";
var school_distance_to_DoE_in_mn_td = "school_distance_to_DoE_in_mn_td";
var school_type_of_school_td = "school_type_of_school_td";
var school_road_quality_td = "school_road_quality_td";
var school_when_accessible_by_road_td = "school_when_accessible_by_road_td";

var school_code_span = "school_code_span";
var school_name_khmer_span = "school_name_khmer_span";
var school_name_latin_span = "school_name_latin_span";
var school_province_span = "school_province_span";
var school_district_span = "school_district_span";
var school_commune_code_span = "school_commune_code_span";
var school_commune_name_span = "school_commune_name_span";
var school_village_code_span = "school_village_code_span";
var school_village_name_span = "school_village_name_span";
var school_geolocation_x_span = "school_geolocation_x_span";
var school_geolocation_y_span = "school_geolocation_y_span";
var school_has_primary_span = "school_has_primary_span";
var school_has_lower_secondary_span = "school_has_lower_secondary_span";
var school_has_upper_secondary_span = "school_has_upper_secondary_span";
var school_has_vocational_training_span = "school_has_vocational_training_span";
var school_last_visited_by_DTMT_span = "school_last_visted_by_DTMT_span";
var school_distance_to_DoE_in_km_span = "school_distance_to_DoE_span";
var school_distance_to_DoE_in_mn_span = "school_distance_to_DoE_in_mn_span";
var school_type_of_school_span = "school_type_of_school_span";
var school_road_quality_span = "school_road_quality_span";
var school_when_accessible_by_road_span = "school_when_accessible_by_road_span";


function loadData(action) {
    var search_string = $("#search-input").val();
    var school_cat;
    school_cat = '';
    if (organization === 'PED' || organization === 'SED') {
        school_cat = $("#select-school-cat").val();
    }
    $("#loading").show();
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        async: false,
        data: {page: "school", action: action, search_string: search_string, school_cat: school_cat},
        success: function (result) {
            // Redirect to home page
            var data = JSON.parse(result);
            var i;
            $("#data-list").html("");
            for (i = 0; i < data.length; i++) {
                var string;
                var checkbox = "<input class='school-check' type='checkbox' onchange='isSelect()' value='" + data[i].school_code + "' style='margin-right:10px;'>";
                string = "<tr>" +
                        "<td class='" + school_code_td + "'>" + checkbox + "<span class='" + school_code_span + "'>" + data[i].school_code + "</span></td>" +
                        "<td class='" + school_name_khmer_td + "'><span class='" + school_name_khmer_span + "'>" + data[i].school_name_khmer + "</span></td>" +
                        "<td class='" + school_name_latin_td + "'><span class='" + school_name_latin_span + "'>" + data[i].school_name_latin + "</span></td>" +
                        "<td class='" + school_commune_name_td + "'><span class='" + school_commune_name_span + "'>" + data[i].commune_name + "</span></td>" +
                        "<td class='" + school_village_name_td + "'><span class='" + school_village_name_span + "'>" + data[i].village_name + "</span></td>" +
                        "<td class='" + school_distance_to_DoE_in_km_td + "'><span class='" + school_distance_to_DoE_in_km_span + "'>" + (data[i].distance_to_DoE_in_km === '0' ? '' : data[i].distance_to_DoE_in_km) + "</span></td>" +
                        "<td class='" + school_distance_to_DoE_in_mn_td + "'><span class='" + school_distance_to_DoE_in_mn_span + "'>" + (data[i].distance_to_DoE_in_minute === '0' ? '' : data[i].distance_to_DoE_in_minute) + "</span></td>" +
                        "<td class='" + school_road_quality_td + "'><span class='" + school_road_quality_span + "'>" + data[i].road_quality + "</span></td>" +
                        "<td class='" + school_when_accessible_by_road_td + "'><span class='" + school_when_accessible_by_road_span + "'>" + data[i].when_accessible_by_road + "</span></td>" +
                        (right_edit === '1' ? '<td>' + default_option + '</td>' : '') +
                        "</tr>";
                $("#data-list").append(string);
            }
            $("#loading").hide();
        },
        error: function (jqXHR) {
            $("#loading").hide();
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Unable to load data!');
            }
        }
    });
}



function edit(row) {
    row_id = $(row).parent().parent();
    school_code = $(row_id).children("." + school_code_td).children("." + school_code_span).html();
    school_name_khmer = $(row_id).children("." + school_name_khmer_td).children("." + school_name_khmer_span).html();
    school_name_latin = $(row_id).children("." + school_name_latin_td).children("." + school_name_latin_span).html();
    school_district = $(row_id).children("." + school_district_td).children("." + school_district_span).html();
    school_commune_name = $(row_id).children("." + school_commune_name_td).children("." + school_commune_name_span).html();
    school_village_name = $(row_id).children("." + school_village_name_td).children("." + school_village_name_span).html();
    school_distance_to_DoE_in_km = $(row_id).children("." + school_distance_to_DoE_in_km_td).children("." + school_distance_to_DoE_in_km_span).html();
    school_distance_to_DoE_in_mn = $(row_id).children("." + school_distance_to_DoE_in_mn_td).children("." + school_distance_to_DoE_in_mn_span).html();
    school_road_quality = $(row_id).children("." + school_road_quality_td).children("." + school_road_quality_span).html();
    school_when_accessible_by_road = $(row_id).children("." + school_when_accessible_by_road_td).children("." + school_when_accessible_by_road_span).html();
    $("#btn-save-update").html("Update");
    $("#btn-save-update").attr("name", "update");
    // set field
    $("#txt-code").val(school_code);
    $("#txt-code").attr("disabled", true);
    $("#txt-name").val(school_name_khmer);
    $("#txt-name-latin").val(school_name_latin);
    $("#txt-commune-code").val(school_commune_name);
    $("#txt-village-code").val(school_village_name);

//    commune = JSON.parse(getData("commune", ["commune_code", "commune_name"], "district_code=" + school_district));
//    $("#select-commune-code").html("");
//    for (i = 0; i < commune.length; i++) {
//        $("#select-commune-code").append("<option value='" + commune[i].commune_code + "'" + (commune[i].commune_code === school_commune_code ? "selected" : "") + ">" + commune[i].commune_code + "-" + commune[i].commune_name + "</option>");
//    }
//    village = JSON.parse(getData("village", ["village_code", "village_name"], "commune_code=" + school_commune_code));
//    $("#select-village-code").html("");
//    for (i = 0; i < village.length; i++) {
//        $("#select-village-code").append("<option value='" + village[i].village_code + "'" + (village[i].village_code === school_village_code ? "selected" : "") + ">" + village[i].village_code + "-" + village[i].village_name + "</option>");
//    }

    $("#txt-distance-to-DoE-km").val(school_distance_to_DoE_in_km);
    $("#txt-distance-to-DoE-mn").val(school_distance_to_DoE_in_mn);
    $("#select-road-quality").html("");
    for (i = 0; i < road_type.length; i++) {
        $("#select-road-quality").append("<option value='" + road_type[i].type + "'" + (road_type[i].type === school_road_quality ? "selected" : "") + ">" + road_type[i].type + "</option>");
    }
    $("#select-when-accessible").html("");
    for (i = 0; i < when_access.length; i++) {
        $("#select-when-accessible").append("<option value='" + when_access[i].time + "'" + (when_access[i].time === school_when_accessible_by_road ? "selected" : "") + ">" + when_access  [i].time + "</option>");
    }
}
var update = function ()
{
    school_code = $("#txt-code").val();
//    school_name_khmer = $("#txt-name").val();
//    school_name_latin = $("#txt-name-latin").val();
//    school_commune_code = $("#select-commune-code").val();
//    school_village_code = $("#select-village-code").val();
    school_distance_to_DoE_in_km = $("#txt-distance-to-DoE-km").val();
    school_distance_to_DoE_in_mn = $("#txt-distance-to-DoE-mn").val();
    school_road_quality = $("#select-road-quality").val();
    school_when_accessible_by_road = $("#select-when-accessible").val();

    var data = [school_distance_to_DoE_in_km,
        school_distance_to_DoE_in_mn, school_road_quality,
        school_when_accessible_by_road, school_code];
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        data: {page: "school", action: "update", data: data
        },
        success: function (result)
        {
            $('#edit').modal('toggle');
// update only current row
            var school = JSON.parse(result);
            alertMessage('success', 'School has been updated!');
            $(row_id).children("." + school_distance_to_DoE_in_km_td).children("." + school_distance_to_DoE_in_km_span).html(school[0].distance_to_DoE_in_km);
            $(row_id).children("." + school_distance_to_DoE_in_mn_td).children("." + school_distance_to_DoE_in_mn_span).html(school[0].distance_to_DoE_in_minute);
            $(row_id).children("." + school_road_quality_td).children("." + school_road_quality_span).html(school[0].road_quality);
            $(row_id).children("." + school_when_accessible_by_road_td).children("." + school_when_accessible_by_road_span).html(school[0].when_accessible_by_road);
        },
        error: function (jqXHR)
        {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                $('#edit').modal('toggle');
                alertMessage('fail', jqXHR.responseText);
            }
        }
    });
};

var search = function () {
    loadData('view');

};

var sessionOperation = function (location, type) {
    var value_province;
    var value_district;
    value_province = $("#select-province").val();
    if (location === 'province') {
        var dist = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + value_province));
        $("#select-district").html("");
        for (i = 0; i < dist.length; i++) {
            $("#select-district").append("<option value='" + dist[i].district_code + "'>" + dist[i].district_code + "-" + dist[i].district_name + "</option>");
        }
    }
    value_district = $("#select-district").val();
    $.ajax({
        url: "session_operation.php",
        type: "POST",
        async: false,
        data: {type: type, pro_code: value_province, dist_code: value_district},
        success: function () {
            loadData("view");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Internal Server Error!');
            }
        }
    });
};

var deleteSchool = function () {
    var obj = $(".school-check:checked");
    array_school = [];
    for (i = 0; i < obj.length; i++) {
        array_school.push(obj[i].value);
    }
    if (array_school.length !== 0 && confirm("Are you sure you want to delete?")) {
        // $(".loading").show();
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            data: {page: "school", action: "delete", school_list: array_school},
            success: function () {
                loadData('view');
                alert((array_school.length === 1 ? " 1 record has" : array_school.length + " records have") + " been deleted!");
                $("#check-all").prop("checked", false);
                $("#delete").hide();
            },
            error: function (jqXHR) {
                if (jqXHR.status === 401) {
                    $('#timeout').modal('toggle');
                }
                else {
                    alertMessage('fail', 'Unable to delete!');
                }
            },
            complete: function () {
                //$(".loading").hide();
            }
        });
    }
};


var check = function () {
    $(".school-check").prop('checked', true);
};
var uncheck = function () {
    $(".school-check").prop('checked', false);
};
var isSelect = function () {
    if ($(".school-check:checked").length === 0)
    {
        $("#delete").hide();
    }
    else
    {
        $("#delete").show();
    }
};

var showSelectDistrict = function () {
    var def_distr = $("#default-district").html();
    $.ajax({
        url: "session_operation.php",
        type: "POST",
        async: false,
        data: {type: "load-district"},
        success: function (result) {
            result = JSON.parse(result);
            $("#select-district").html("");
            for (i = 0; i < result.length; i++) {
                $("#select-district").append("<option value='" + result[i].district_code + "'" + (result[i].district_code === def_distr ? "selected" : "") + ">" + result[i].district_code + "-" + result[i].district_name + "</option>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else{
                alertMessage('fail','Internal Server Error!');
            }
        }
    });
};

var showSelectProvince = function () {
    var def_pro = $("#default-province").html();
    $.ajax({
        url: "session_operation.php",
        type: "POST",
        async: false,
        data: {type: "load-province"},
        success: function (result) {
            result = JSON.parse(result);
            for (i = 0; i < result.length; i++) {
                $("#select-province").append("<option value='" + result[i].province_code + "'" + (result[i].province_code === def_pro ? "selected" : "") + ">" + result[i].province_code + "-" + result[i].province_name + "</option>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else{
                alertMessage('fail','Internal Server Error!');
            }
        }
    });
};

$(document).ready(function () {
    $("#check-all").change(function () {
        if (this.checked) {
            check();
        }
        else {
            uncheck();
        }
        isSelect();
    });
    organization = $("#organization").html();
    right_edit = $("#right-to-edit-data").html();
    setHeight();
    loadData('view');
    // get only province list
//    province = JSON.parse(getData("province", ["province_code", "province_name"]));
//    default_district = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=1"));
//    default_commune = JSON.parse(getData("commune", ["commune_code", "commune_name"], "district_code=102"));
});
