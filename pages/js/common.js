var getData = function (classes, field, where) {
    var results = "";
    $.ajax({
        url: "data_fetching.php",
        type: "GET",
        data: {"class": classes, "field": field, "id": where},
        async: false,
        success: function (result) {
            results = result;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
        }
    });
    return results;
};

var alertMessage = function (type, message) {
    $("#message").html(message);
    if (type === 'success') {
        $("#message").css({"color": "green"});
    }
    else if (type === 'fail') {
        $("#message").css({"color": "red"});
    }
    $('#info').modal('toggle');
};


var loadNew = function (table) {
    var district;
    var commune;
    var village;
    var value;
    if (table === 'province') {// refress both district and commune
        value = $("#select-province-code").val();
        if (value === 'empty') {
            $("#select-district-code").html("");
            $("#select-district-code").append("<option value=''>---SELECT---</option>");
        }
        else {
            district = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + value));
            $("#select-district-code").html("");
            for (i = 0; i < district.length; i++) {
                $("#select-district-code").append("<option value='" + district[i].district_code + "'>" + district[i].district_code + "-" + district[i].district_name + "</option>");
            }
        }
        $("#select-commune-code").html("");
        $("#select-commune-code").append("<option value=''>---SELECT---</option>");
        $("#select-village-code").html("");
        $("#select-village-code").append("<option value=''>---SELECT---</option>");
    }
    else if (table === 'district') {// refress both district and commune
        value = $("#select-district-code").val();
        commune = JSON.parse(getData("commune", ["commune_code", "commune_name"], "district_code=" + value));
        $("#select-commune-code").html("");
        for (i = 0; i < commune.length; i++) {
            $("#select-commune-code").append("<option value='" + commune[i].commune_code + "'>" + commune[i].commune_code + "-" + commune[i].commune_name + "</option>");
        }
        $("#select-village-code").html("");
        $("#select-village-code").append("<option value=''>---SELECT---</option>");
    }
    else if (table === 'commune') {// refress both district and commune
        value = $("#select-commune-code").val();
        $("#txt-commune-name").val(value);
        village = JSON.parse(getData("village", ["village_code", "village_name"], "commune_code=" + value));
        $("#select-village-code").html("");
        for (i = 0; i < village.length; i++) {
            $("#select-village-code").append("<option value='" + village[i].village_code + "'>" + village[i].village_code + "-" + village[i].village_name + "</option>");
        }
        $("#txt-village-name").val(village[0].village_name);
    }
};

