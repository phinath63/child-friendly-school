/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var default_province_list = [];
var district_list = [];
var valid_user_id = false;
var full_name, user_id, password, organization, province, district, DTMT_member, DTMT_type,
        created_by, enter_data, edit_data, access_statistic, school_distance,
        new_user, edit_user;

var fillLocation = function (location) {
    if (location === 'province') {
        $("#select-province").html("");
        for (i = 0; i < default_province_list.length; i++) {
            $("#select-province").append("<option value='" + default_province_list[i].province_code + "'>" + default_province_list[i].province_code + "-" + default_province_list[i].province_name + "</option>");
        }
    }
    else if (location === 'district' && $("#select-organization").val() === 'DoE') {
        var province_code = $("#select-province").val();
        district_list = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + province_code));
        $("#select-district").html("");
        for (i = 0; i < district_list.length; i++) {
            $("#select-district").append("<option value='" + district_list[i].district_code + "'>" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
        }
    }
    fillDTMTType();
};

var isDTMT = function () {
    $("#dtmt-type").toggle('slow', 'linear');
    if ($("#select-dtmt-member").val() === '1') {
        fillDTMTType();
    }
};

var fillDTMTType = function () {
    if ($("#select-dtmt-member").val() === '1') {// find out if it is PED, SED, or PoE
        var organization = $("#select-organization").val();
        if (organization === 'PED' || organization === 'SED') {// list all the district
            district_list = JSON.parse(getData("district", ["district_code", "district_name"]));
            $("#select-dtmt-district").html("");
            for (i = 0; i < district_list.length; i++) {
                $("#select-dtmt-district").append("<option value='" + district_list[i].district_code + "'>" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
            }
        }
        else if (organization === 'PoE') {// list all the district
            var province_code = $("#select-province").val();
            district_list = JSON.parse(getData("district", ["district_code", "district_name"], "province_code=" + province_code));
            $("#select-dtmt-district").html("");
            for (i = 0; i < district_list.length; i++) {
                $("#select-dtmt-district").append("<option value='" + district_list[i].district_code + "'>" + district_list[i].district_code + "-" + district_list[i].district_name + "</option>");
            }
        }
        else if (organization === 'DoE') {
            $("#select-dtmt-district").html("");
            var district = $("#select-district").val();
            $("#select-dtmt-district").append("<option value='" + district + "'>" + district + "</option>");
        }
    }
};

var checkOrganization = function () {
    var organization = $("#select-organization").val();
    if (organization === 'PED' || organization === 'SED') {// hide province and istrict
        $("#province").hide(500);
        $("#district").hide(500);
    }
    else if (organization === 'PoE') {// hide district
        $("#province").show(500);
        $("#district").hide(500);
    }
    else {
        $("#province").show(500);
        $("#district").show(500);
        fillLocation('district');
    }
    fillDTMTType();
};

var checkFill = function () {
    /*
     * 1 check for empty
     * 2 check for valid user id
     * 3 check for matched password
     */
    var boolean;
    boolean = true;
    var fullName = $("#txt-full-name").val();
    var user_id = $("#txt-user-id").val();
    var password = $("#txt-password").val();
    var conPassword = $("#txt-con-password").val();
    if (fullName.length === 0) {
        $("#lb-full-name").html("*Enter Full Name");
        boolean = false;
    }
    else {
        $("#lb-full-name").html("");
    }
    if (user_id.length < 3) {
        $("#lb-user-id").html("*User Id must be at least 3 characters");
        boolean = false;
    }
    else {
        // check is user id is existed
        if (valid_user_id) {
            $("#lb-user-id").html("");
        }
        else {
            $("#lb-user-id").html("*User Id is already existed");
            boolean = false;
        }

    }
    if (password.length < 6) {
        $("#lb-password").html("*Password length must be at least 6 characters");
        boolean = false;
    }
    else {
        if (password !== conPassword) {
            $("#lb-password").html("*Password is not matched");
            boolean = false;
        }
        else {
            $("#lb-password").html("");
        }
    }
    return boolean;
};

var checkid = function () {
    var id = $("#txt-user-id").val();
    if (id.length >= 3) {
        $("#img-user-id").show();
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            data: {page: "user", action: "check-id", id: id},
            async: false,
            success: function () {
                valid_user_id = true;
                $("#lb-user-id").html("");
            },
            error: function () {
                valid_user_id = false;
                $("#lb-user-id").html("*User Id is already existed");
            },
            complete: function () {
                $("#img-user-id").hide();
            }
        });
    }
};

var register = function () {
    if (checkFill()) {
        // all fields are verified
        var data = [];
        full_name = $("#txt-full-name").val();
        user_id = $("#txt-user-id").val();
        password = $("#txt-password").val();
        organization = $("#select-organization").val();
        province = $("#select-province").val();
        district = $("#select-district").val();
        DTMT_member = $("#select-dtmt-member").val();
        DTMT_type = $("#select-dtmt-district").val();
        created_by = $("#user_id").html();
        enter_data = $("#select-right-to-enter-data").val();
        edit_data = $("#select-right-to-edit-data").val();
        access_statistic = $("#select-right-to-access-statistic").val();
        school_distance = $("#select-right-to-enter-school-distance").val();
        new_user = $("#select-right-to-create-new-user").val();
        edit_user = $("#select-right-to-edit-user").val();
        data = [full_name, user_id, password, organization, province, district,
            DTMT_member, DTMT_type, created_by, enter_data, edit_data,
            access_statistic, school_distance, new_user, edit_user];
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            data: {page: "user", action: "register", data: data},
            async: false,
            success: function () {
                alertMessage('success', 'New user has been registered!')
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    $('#timeout').modal('toggle');
                }
                else {
                    alertMessage('fail', jqXHR.responseText);
                }
            }
        });

    }
    else {
        console.log("fail");
    }
};

$(document).ready(function () {
    //load new province and district at same time;
    default_province_list = JSON.parse(getData("province", ["province_code", "province_name"]));
    fillLocation('province');
    fillLocation('district');
    $("#register").click(function () {
        register();
    });

});

