var school_type = [{type: 'Close'}, {type: 'Medium'}, {type: 'Remote'}];
var yes_no = [{id: 0, name: 'No'}, {id: 1, name: 'Yes'}];
var road_type = [{type: 'Dirt'}, {type: 'Pave'}, {type: 'Only Moto'}];
var when_access = [{time: 'All Year'},
    {time: 'Road cute every year during rain'},
    {time: 'Road cute some year during rain'},
    {time: 'Road cut very rarely during rain'},
    {time: 'No Access by road'}];

// location
var province = [];
var valid_school_code = false;

var addNew = function () {
    $("#txt-code").val("");
    $("#txt-name").val("");
    $("#txt-name-latin").val("");
    $("#select-province-code").html("");
    $("#select-province-code").append("<option value='0'>---SELECT---</option>");
    for (i = 0; i < province.length; i++) {
        $("#select-province-code").append("<option value='" + province[i].province_code + "'>" + province[i].province_code + "-" + province[i].province_name + "</option>");
    }
    $("#select-district-code").html("");
    $("#select-district-code").append("<option value='0'>---SELECT---</option>");
    $("#select-commune-code").html("");
    $("#select-commune-code").append("<option value='0'>---SELECT---</option>");
    $("#select-village-code").html("");
    $("#select-village-code").append("<option value='0'>---SELECT---</option>");
    $("#txt-long").val("");
    $("#txt-lat").val("");
    $("#select-has-primary").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-has-primary").append("<option value='" + yes_no[i].id + "'>" + yes_no[i].name + "</option>");
    }
    $("#select-has-lower-secondary").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-has-lower-secondary").append("<option value='" + yes_no[i].id + "'>" + yes_no[i].name + "</option>");
    }
    $("#select-has-upper-secondary").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-has-upper-secondary").append("<option value='" + yes_no[i].id + "'>" + yes_no[i].name + "</option>");
    }
    $("#select-has-vocational").html("");
    for (i = 0; i < yes_no.length; i++) {
        $("#select-has-vocational").append("<option value='" + yes_no[i].id + "'>" + yes_no[i].name + "</option>");
    }
    $("#txt-last-visited-DTMT").val("");
    $("#txt-distance-to-DoE-km").val("");
    $("#txt-distance-to-DoE-mn").val("");
    $("#select-school-type").html("");
    for (i = 0; i < school_type.length; i++) {
        $("#select-school-type").append("<option value='" + school_type[i].type + "'>" + school_type[i].type + "</option>");
    }

    $("#select-road-quality").html("");
    for (i = 0; i < road_type.length; i++) {
        $("#select-road-quality").append("<option value='" + road_type[i].type + "'>" + road_type[i].type + "</option>");
    }
    $("#select-when-accessible").html("");
    for (i = 0; i < when_access.length; i++) {
        $("#select-when-accessible").append("<option value='" + when_access[i].time + "'>" + when_access  [i].time + "</option>");
    }
};

var checkField = function () {

};

var checkSchoolCode = function () {
    var code;
    code = $("#txt-code").val();
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        data: {page: "school", action: "check-id", id: code},
        async: false,
        success: function () {
            valid_school_code = true;
            $("#lb-user-id").html("");
        },
        error: function () {
            valid_school_code = false;
            $("#lb-user-id").html("*User Id is already existed");
        },
        complete: function () {
            $("#img-user-id").hide();
        }
    });
};

var save = function ()
{
    school_code = $("#txt-code").val();
    school_name_khmer = $("#txt-name").val();
    school_name_latin = $("#txt-name-latin").val();
    school_province = $("#select-province-code").val();
    school_district = $("#select-district-code").val();
    school_commune = $("#select-commune-code").val();
    school_village = $("#select-village-code").val();
    school_geolocation_x = $("#txt-lat").val();
    school_geolocation_y = $("#txt-long").val();
    school_has_primary = $("#select-has-primary").val();
    school_has_lower_secondary = $("#select-has-lower-secondary").val();
    school_has_upper_secondary = $("#select-has-upper-secondary").val();
    school_has_vocational_training = $("#select-has-vocational").val();
    school_last_visited_by_DTMT = $("#txt-last-visited-DTMT").val();
    school_distance_to_DoE_in_km = $("#txt-distance-to-DoE-km").val();
    school_distance_to_DoE_in_mn = $("#txt-distance-to-DoE-mn").val();
    school_type_school = $("#select-school-type").val();
    school_road_quality = $("#select-road-quality").val();
    school_when_accessible_by_road = $("#select-when-accessible").val();

    var data = [school_code, school_name_khmer, school_name_latin, school_province,
        school_district, school_commune, school_village, school_geolocation_x,
        school_geolocation_y, school_has_primary, school_has_lower_secondary,
        school_has_upper_secondary, school_has_vocational_training,
        school_last_visited_by_DTMT, school_distance_to_DoE_in_km,
        school_distance_to_DoE_in_mn, school_type_school, school_road_quality,
        school_when_accessible_by_road];
    $.ajax({
        url: "page_proccessing.php",
        type: "POST",
        data: {page: "school", action: "save", data: data
        },
        success: function ()
        {
            $('#add').modal('toggle');
            alertMessage('success', 'New User has been registered!');
            loadData('view');
        },
        error: function ()
        {
            $('#add').modal('toggle');
            alertMessage('fail', 'Internal Error!');

        }
    });
};

$(document).ready(function () {
    province = JSON.parse(getData("province", ["province_code", "province_name"]));
    addNew();
    loading();
});


