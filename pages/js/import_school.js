$(document).ready(function () {
    setHeight();
});


var fileUpload = function (param) {
    var xhr = new XMLHttpRequest();
    //var fd = document.getElementById(param.formId).getFormData();
    var form = document.getElementById(param.formId);
    var fd = new FormData(form);

    xhr.onreadystatechange = function ()
    {
        if (xhr.readyState === 4 && xhr.status === 200)
        {
            param.success(xhr.responseText);
        }
        else if (xhr.readyState === 4 && xhr.status === 500)
        {
            param.error(xhr.responseText);
        }
        else if (xhr.readyState === 4 && xhr.status === 401)
        {
            param.timeout(401);
        }
    };

    xhr.open("POST", param.url);
    xhr.send(fd);
};

var uploadFile = function (forId) {
    $("#loading").show();
    fileUpload({
        formId: forId,
        url: 'import_data.php',
        success: function (text) {
            $("#loading").hide();
            alertMessage('success', 'Data has been imported');
        },
        error: function (text) {
            $("#loading").hide();
            alertMessage('fail', text);
        },
        timeout: function (http_response) {
            $("#loading").hide();
            $('#timeout').modal('toggle');
        }
    });
};
