var school_list = [];
var default_province_list;
var organization;
var district_code;
var school_type;
var number_of_school;
var number_of_dtmt;
var start_date;
var end_date;
var year;
var quarter_number;

var planning_btn = "<button class='btn btn-primary btn-sm' onclick='registerPlan()'>Do Planning</button>";
var fillDate = function () {
    var min = $("#txt-start-date").val();
    var max = $("#txt-end-date").val();
    $(".date").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: min,
        maxDate: max
    });
};

var showSelectDistrict = function () {
    var def_distr = $("#default-district").html();
    $.ajax({
        url: 'page_proccessing.php',
        type: 'POST',
        data: {page: 'planning', action: 'select-district'},
        async: false,
        success: function (data, textStatus, jqXHR) {
            var str;
            data = JSON.parse(data);
            $("#select-district-code").html('');
            for (i = 0; i < data.length; i++) {
                str = "<option value='" + data[i].district_code + "'" + (data[i].district_code === def_distr ? "selected" : "") + ">" + data[i].district_code + "-" + data[i].district_name + "</option>";
                $("#select-district-code").append(str);
            }
        },
        error: function (jqXHR) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                alertMessage('fail', 'Unable to load district!');
            }
        }
    });
};

var isValidSelection = function () {
    number_of_school = $("#txt-number-of-school").val();
    number_of_dtmt = $("#txt-number-of-dtmt").val();
    var valid = true;
    if (number_of_school < 1 || isNaN(number_of_school)) {
        $("#span-number-of-school-error").show();
        valid = false;
    }
    else {
        $("#span-number-of-school-error").hide();
    }
    if (number_of_dtmt < 1 || isNaN(number_of_dtmt)) {
        $("#span-number-of-dtmt-error").show();
        valid = false;
    }
    else {
        $("#span-number-of-dtmt-error").hide();
    }
    return valid;
};
var calculateSchoolCat = function () {
    if (isValidSelection()) {


        district_code = $("#select-district-code").val();
        start_date = $("#txt-start-date").val();
        end_date = $("#txt-end-date").val();
        year = $("#select-year").val();
        quarter_number = $("#select-quarter").val();
        $("#loading").show();
        $.ajax({
            url: 'page_proccessing.php',
            type: 'POST',
            data: {page: 'planning', action: 'school-type', district_code: district_code, number_of_school: number_of_school},
            success: function (data, textStatus, jqXHR) {
                var school_cat = JSON.parse(data);
                var school_near = getSchool('Near');
                var school_distant = getSchool('Distant');
                var school_remote = getSchool('Remote');
                var no = 1;
                var input_date = "<input class='form-control input-sm date txt-mission-date' type='text' autocomplete = 'on' id='' placeholder='dd-mm-yyyy'>";
                var shift = "<select class='form-control input-sm select-shift'>" +
                        "<option value='Morning'>Morning</option>" +
                        "<option value='Evening'>Evening</option>" +
                        "</select>";
                // fill the table;
                $("#data-list").html('');
                for (i = 0; i < school_cat.Near; i++) {
                    var string = "<tr>" +
                            "<td>" + (no++) + "</td>" +
                            "<td>" + school_near + "</td>" +
                            "<td>" + input_date + "</td>" +
                            "<td>" + shift + "</td>" +
                            "<td class='school-type'>Near</td>" +
                            "</tr>";
                    $("#data-list").append(string);
                }
                for (i = 0; i < school_cat.Distant; i++) {
                    var string = "<tr>" +
                            "<td>" + (no++) + "</td>" +
                            "<td>" + school_distant + "</td>" +
                            "<td>" + input_date + "</td>" +
                            "<td>" + shift + "</td>" +
                            "<td class='school-type'>Distant</td>" +
                            "</tr>";
                    $("#data-list").append(string);
                }
                for (i = 0; i < school_cat.Remote; i++) {
                    var string = "<tr>" +
                            "<td>" + (no++) + "</td>" +
                            "<td>" + school_remote + "</td>" +
                            "<td>" + input_date + "</td>" +
                            "<td>" + shift + "</td>" +
                            "<td class='school-type'>Remote</td>" +
                            "</tr>";
                    $("#data-list").append(string);
                }
                fillDate();
                $(".planning").html(planning_btn);
                $("#loading").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#loading").hide();
                if (jqXHR.status === 401) {
                    $('#timeout').modal('toggle');
                }
                else {
                    alertMessage('fail', jqXHR.responseText);
                }
            }
        });
    }
};

var getSchool = function (cat) {
    var district_code = $("#select-district-code").val();
    var string = '';
    $.ajax({
        url: 'page_proccessing.php',
        type: 'POST',
        data: {page: 'planning', action: 'get-school', district_code: district_code, cat: cat},
        async: false,
        success: function (data, textStatus, jqXHR) {
            var data = JSON.parse(data);
            string += "<select class='form-control input-sm select-school-code'>";
            for (i = 0; i < data.length; i++) {
                string += "<option value='" + data[i].school_code + "'>" + data[i].school_code + "-" + data[i].school_name_latin + "</option>";
            }
            string += "</select>";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alertMessage('fail', jqXHR.responseText);
        }
    });
    return string;
};

var registerPlan = function () {
    var school_code = $(".select-school-code");
    var mission_date = $(".txt-mission-date");
    var shift = $(".select-shift");
    var type_of_school = $(".school-type");
    var school_list = [];
    var mission_list = [];
    var shift_list = [];
    var type_list = [];
    for (i = 0; i < school_code.length; i++) {
        school_list.push(school_code[i].value);
        mission_list.push(mission_date[i].value);
        shift_list.push(shift[i].value);
        type_list.push(type_of_school[i].innerHTML);
    }
    $.ajax({
        url: 'page_proccessing.php',
        type: 'POST',
        data: {page: 'planning', action: 'register-plan',
            district_code: district_code, number_of_school: number_of_school,
            number_of_dtmt: number_of_dtmt, start_date: start_date, end_date: end_date,
            year: year, quarter: quarter_number, school_code: school_list, mission_date: mission_list,
            shift: shift_list, type: type_list},
        async: false,
        success: function (data, textStatus, jqXHR) {
            alertMessage('success', 'New Plan has been inserted');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#timeout').modal('toggle');
            }
            else {
                $('#edit').modal('toggle');
                alertMessage('fail', jqXHR.responseText);
            }
        }
    });

};
var fillYear = function () {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var y = d.getFullYear();
    $("#txt-start-date").val(day + "-" + month + "-" + y);
    d.setMonth(14);
    $("#txt-end-date").val(d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear());
    $("#select-year").html('');
    for (i = y - 5; i < y + 5; i++) {
        var str = "<option value='" + i + "'" + (i === y ? 'selected' : '') + ">" + i + "</option>";
        $("#select-year").append(str);
    }
};


$(document).ready(function () {
    showSelectDistrict();
    setHeight();
    $(".date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    fillYear();
});