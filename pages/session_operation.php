<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION["username"])) {
    http_response_code(401);
    die("Session time out!");
}
require './config.php';
DB::getInstance()->query("SET NAMES utf8"); // convert data from database to utf8
$db = DB::getInstance();
$type = $_POST["type"];
switch ($type) {
    case 'change-dtmt':
        if (strcmp($_SESSION["organization"], 'PED') === 0 || strcmp($_SESSION["organization"], 'SED') === 0) {
            $province_code = $_POST["pro_code"];
            $_SESSION['province'] = $province_code;
            $district_code = $_POST["dist_code"];
            $_SESSION["DTML-district"] = $_SESSION['district'] = $district_code;
        } else if (strcmp($_SESSION["organization"], 'PoE') === 0) {
            $district_code = $_POST["dist_code"];
            $_SESSION["DTML-district"] = $_SESSION['district'] = $district_code;
        }
        break;
    case 'load-province':
        if (strcmp($_SESSION["organization"], 'PED') === 0 || strcmp($_SESSION["organization"], 'SED') === 0) {
            $sql = "select province_code, province_name from province";
            $db->query($sql);
            echo json_encode($db->getResults());
        }
        break;
    case 'load-district':
        if (strcmp($_SESSION["organization"], 'DoE') !== 0) {
            $sql = "select district_code, district_name from district where province_code= ?";
            $db->query($sql, array($_SESSION["province"]));
            echo json_encode($db->getResults());
        }
        break;
    default :
        break;
}

