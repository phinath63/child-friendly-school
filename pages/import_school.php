<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<style>
</style>
<script src="js/common.js">
</script>
<script src="js/import_school.js">
</script>

<div class="col-sm-10 content">
    <div id='loading' style="width: 100%; position: absolute; display: none;">
        <div class='uil-ring-css' style='-webkit-transform:scale(0.99); margin: 0 auto; top: 30%'><div></div></div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row select-tool">

                </div>
                <div class="row data-list" style="overflow-y: auto;">
                    <div class="level-operation form-horizontal">
                        
                        <form  id="upload">
                                <label class="">Input file</label>
                                <input class="btn btn-default" type="file" name="file">
                        </form>
                        <br>
                        <button class="btn btn-primary" onclick="uploadFile('upload')">Import</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
</div>
</body>
</html>
