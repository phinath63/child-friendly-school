<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (strcmp($_SESSION["create-user"], "0") === 0) {
    echo "You do not have authority to create new user!!!";
    exit();
}
require '../template/header.php';
require '../template/sidebar.php';
?>
<style>
    .red{
        color:red;
    }
</style>
<script src="js/common.js">
</script>
<script src="js/user_registration.js"></script>
<span id="user_id" style="display:none"><?php echo $_SESSION["username"]; ?></span>

<div class="col-sm-10">
    <div class="container-fluid">
        <div class="level-operation form-horizontal" style="border: 1px solid blueviolet; padding: 20px;">
            <div class="form-group">
                <label  class="col-sm-5" for="full-name">Full Name:<span id='lb-full-name' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" placeholder="Enter Full Name" name="full-name" id="txt-full-name">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="user-id">User Id:<span id='lb-user-id' class='red pull-right'></span><img id="img-user-id" class="pull-right" src="media/picture/checking_loader.gif" style="display: none"></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" placeholder="Enter User Id" name="user-id" id="txt-user-id" onchange="checkid();">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="password">Password:<span id='lb-password' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="password" placeholder="Enter Password" name="password" id="txt-password">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="password">Confirm Password:<span id='lb-con-password' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="password" placeholder="Enter Password Again" name="con-password" id="txt-con-password">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="organization">Organization:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="organization" id="select-organization" onchange="checkOrganization()">
                        <option value="DoE">DoE</option>
                        <option value="PoE">PoE</option>
                        <option value="PED">PED</option>
                        <option value="SED">SED</option>
                    </select>
                </div>
            </div>
            <div id='province' class="form-group">
                <label  class="col-sm-5" for="province">Province:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="province" id="select-province" onchange="fillLocation('district')"></select>
                </div>
            </div>
            <div id='district' class="form-group">
                <label  class="col-sm-5" for="district">District:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="district" id="select-district" onchange="fillLocation('')"></select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="DTMT-member">DTMT Member:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="dtmt-member" id="select-dtmt-member" onchange="isDTMT()">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div id='dtmt-type' class="form-group" style="display: none">
                <label  class="col-sm-5" for="DTMT-type">DTML Type:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="dtmt-type" id="select-dtmt-district">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-enter-data">Right to Enter Data:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="enter-data" id="select-right-to-enter-data">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-edit-data">Right to Edit Data:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="edit-data" id="select-right-to-edit-data">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-access-statistic">Right to Access Statistic:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="edit-data" id="select-right-to-access-statistic">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-enter-school-distance">Right to Enter School Distance:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="right-to-enter-school-distance" id="select-right-to-enter-school-distance">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-create-new-user">Right to Create New User:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="right-to-create-new-user" id="select-right-to-create-new-user">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="right-to-edit-user">Right to Edit User:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" type="text" name="right-to-edit-user" id="select-right-to-edit-user">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="row" style="padding-right: 20px;">
                <button id='register' class="btn btn-primary pull-right">Register</button>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>