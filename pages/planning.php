<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<link href="css/jquery-ui.css" rel="stylesheet">
<script src="js/common.js">
</script>
<script src="js/jquery-ui.js"></script>
<script src="js/planning.js">
</script>
<span id="organization" class="hidden"><?php echo $_SESSION["organization"]; ?></span>
<span id="default-province" class="hidden"><?php echo $_SESSION["province"]; ?></span>
<span id="default-district" class="hidden"><?php echo $_SESSION["district"]; ?></span>
<span id="default-dtmt" class="hidden"><?php echo $_SESSION["DTML-district"]; ?></span>
<span id="right-to-enter-data" class="hidden"><?php echo $_SESSION["enter-data"]; ?></span>
<span id="right-to-edit-data" class="hidden"><?php echo $_SESSION["edit-data"]; ?></span>
<span id="right-to-access-statistic" class="hidden"><?php echo $_SESSION["access-statistic"]; ?></span>
<span id="right-to-enter-school-distance" class="hidden"><?php echo $_SESSION["school-distance"]; ?></span>
<span id="right-to-create-new-user" class="hidden"><?php echo $_SESSION["create-user"]; ?></span>
<span id="right-to-create-edit-user" class="hidden"><?php echo $_SESSION["edit-user"]; ?></span>
<div class="col-sm-10 content">
    <div class="panel panel-default select-tool">
        <div class="panel-body">
            <div class="level-operation form-horizontal row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label  class="col-sm-4" for="input-district">School District:</label>
                        <div class="col-sm-8">
                            <select class="form-control input-sm" name="district-code" id="select-district-code">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4" for="number-of-school">Number of School:<span id="span-number-of-school-error" style="color: red;display: none">*require</span></label>
                        <div class="col-sm-8">
                            <input class="form-control input-sm" type="text" name="number-of-school" id="txt-number-of-school">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4" for="period">Period:</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input class="form-control input-sm date" type="text" name="start-date" id="txt-start-date" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input class="form-control input-sm date" type="text" name="end-date" id="txt-end-date" placeholder="dd-mm-yyyy">                        </div>

                        </div>
                    </div>
                </div>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <label  class="col-sm-4" for="input-district">School Category:</label>
                        <div class="col-sm-8">
                            <select class="form-control input-sm" name="school-cat" id="select-school-category">
                                <option value="primary">Primary</option>
                                <option value="lower-secondary">Lower Secondary</option>
                                <option value="pimary-lower-secondary" selected>Primary & Lower Secondary</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4" for="number-of-school">Number of DTMT:<span id="span-number-of-dtmt-error"style="color: red; display: none;">*require</span></label>
                        <div class="col-sm-8">
                            <input class="form-control input-sm" type="text" name="number-of-dtmt" id="txt-number-of-dtmt">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4" for="period">Quarter:</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <select class="form-control input-sm" type="text" name="year" id="select-year" placeholder="Year (yyyy)"></select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="end-date" id="select-quarter">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>  
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-sm" onclick="calculateSchoolCat()">Next</button>
            </div>
        </div>
    </div>
    <div class="panel panel-default data-list" style="overflow-y: auto">
        <div id='loading' style="width: 100%; position: absolute; display: none;">
            <div class='uil-ring-css' style='-webkit-transform:scale(0.99); margin: 0 auto; top: 20%'><div></div></div>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="info">
                        <th>No</th><th>School</th><th>Date</th>
                        <th>Shift</th><th>Type Of School</th>
                    </tr>
                </thead>
                <tbody id="data-list">

                </tbody>
            </table>
            <div class="planning">

            </div>
        </div>
    </div>
</div>