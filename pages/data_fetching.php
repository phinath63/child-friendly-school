<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION["username"])) {
    http_response_code(401);
    die("Session time out!");
}
require './config.php';
$class = $_GET["class"];
$field = $_GET["field"];
$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}
DB::getInstance()->query("SET NAMES utf8"); // convert data from database to utf8
$db = DB::getInstance();
if (strlen($id) == 0) {
    $sql = "SELECT " . implode(",", $field) . " from " . $class;
} else {
    $sql = "SELECT " . implode(",", $field) . " from " . $class . " where " . $id;
}
$db->query("$sql");
echo json_encode($db->getResults());

