<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION["username"])) {
    http_response_code(401);
    die("Session time out!");
}
require_once '../libs/phpExcel/PHPExcel/IOFactory.php';
require './config.php';

function school_category($vcode) {
    $code = (int) $vcode;
    if ($code < 500) {
        return 'has_primary, primary_code, ';
    } else if ($code < 700) {
        return 'has_kindergarten, kindergarten_code, ';
    } else if ($code < 900) {
        return 'has_lower_secondary, lower_secondary_code, ';
    } else {
        return 'has_higher_secondary, higher_secondary_code, ';
    }
}

function school_category_update($vcode) {
    $code = (int) $vcode;
    if ($code < 500) {
        return 'has_primary = ?, primary_code = ? ';
    } else if ($code <700) {
        return 'has_kindergarten = ?, kindergarten_code = ? ';
    } else if ($code < 900) {
        return 'has_lower_secondary = ?, lower_secondary_code = ? ';
    } else {
        return 'has_higher_secondary = ?, higher_secondary_code = ? ';
    }
}

try {
    if (!isset($_FILES['file'])) {
        thow('No file found');
    }
    $file_info = $_FILES['file'];
    $inputFileName = $file_info['name'];
    $file_type = $file_info['type'];
    $file_tmp_name = $file_info['tmp_name'];
    $file_mime_type = mime_content_type($file_tmp_name);
    $inputFileType = PHPExcel_IOFactory::identify($file_tmp_name);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($file_tmp_name);
} catch (Exception $ex) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}
//foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
$worksheet = $objPHPExcel->getSheet(0);
$worksheetTitle = $worksheet->getTitle();
$highestRow = $worksheet->getHighestRow(); // e.g. 10
$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

$db = DB::getInstance();
DB::getInstance()->query("SET NAMES utf8");
$db->beginTransaction();
$remove = array(" ", "?", "!", "@", "#", "$", "%", "^", "&", "*", "`", "-", "=", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
$pre_code = "";
$pre_school_name = "";
$pre_principle_name = "";
for ($row = 4; $row <= $highestRow; ++$row) {
//    for ($col = 0; $col < $highestColumnIndex; ++$col) {
//        $cell = $worksheet->getCellByColumnAndRow($col, $row);
//        $val = $cell->getValue();
//        $data[] = $val;
//    }

    $code = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
    // cut string from $code;
    $school_code = substr($code, -3);
    $village_code = substr($code, 0, strlen($code) - 3); //21010101
    $commune_code = substr($village_code, 0, strlen($village_code) - 2); //210101

    $district_code = substr($commune_code, 0, strlen($commune_code) - 2); //2101
    $province_code = substr($district_code, 0, strlen($district_code) - 2); //21
    $school_name = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
    $school_name_filter = strtolower(str_replace($remove, "", $school_name));
    $principle_name = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
    $principle_name_fillter = strtolower(str_replace($remove, "", $principle_name));
    // check with previous school name;

    $length = strlen($principle_name_fillter);
    $diff = levenshtein($pre_principle_name, $principle_name_fillter);
    $diff_percentage = ($diff / $length) * 100;
    if ((strcmp($school_name_filter, $pre_school_name) === 0 && $diff_percentage < 35 ) || strcmp($school_name_filter, $pre_school_name) !== 0) {
        // update only primary code, or lower secondary code or higher secondary code
        $sql = "UPDATE `school` SET " . school_category_update($school_code) . " WHERE school_code=?";
        $db->dml($sql, array(1, $code, $pre_code));
        echo $row ."up\n";
    } else { // check if the same school
        echo $row ."\n";
        $sql = 'INSERT INTO `school`'
                . '(`school_name_khmer`, '
                . '`school_name_latin`, '
                . '`province_code`, '
                . '`district_code`, '
                . '`commune_code`, '
                . '`village_code`, '
                . '`geolocation_x`, '
                . '`geolocation_y`, '
                . school_category($school_code)
                . '`has_vocational_training`, '
                . '`last_visited_by_DTMT`, '
                . '`distance_to_DoE_in_km`, '
                . '`distance_to_DoE_in_minute`, '
                . '`type_of_school`, '
                . '`road_quality`, '
                . '`when_accessible_by_road`) '
                . 'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        $db->dml($sql, array('', $school_name, $province_code, $district_code, $commune_code,
            $village_code, '', '', 1, $code, 0, date("Y-m-d"), '', '', '', '', ''));
    }
    // if previous and current school name are not the same insert to new row

    if ($db->isError()) {
        $db->rollBack();
        echo $db->getResponseText();
        http_response_code(500);
        break;
    }
    $pre_code = $db->getLastInsertId();
    $pre_school_name = $school_name_filter;
    $pre_principle_name = $principle_name_fillter;
}
$db->commit();

//}
