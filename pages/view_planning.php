<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<script src="js/common.js">
</script>
<script src="js/view_planning.js">
</script>
<span id="organization" class="hidden"><?php echo $_SESSION["organization"]; ?></span>
<span id="default-province" class="hidden"><?php echo $_SESSION["province"]; ?></span>
<span id="default-district" class="hidden"><?php echo $_SESSION["district"]; ?></span>
<span id="default-dtmt" class="hidden"><?php echo $_SESSION["DTML-district"]; ?></span>
<span id="right-to-enter-data" class="hidden"><?php echo $_SESSION["enter-data"]; ?></span>
<span id="right-to-edit-data" class="hidden"><?php echo $_SESSION["edit-data"]; ?></span>
<span id="right-to-access-statistic" class="hidden"><?php echo $_SESSION["access-statistic"]; ?></span>
<span id="right-to-enter-school-distance" class="hidden"><?php echo $_SESSION["school-distance"]; ?></span>
<span id="right-to-create-new-user" class="hidden"><?php echo $_SESSION["create-user"]; ?></span>
<span id="right-to-create-edit-user" class="hidden"><?php echo $_SESSION["edit-user"]; ?></span>
<div class="col-sm-10 content">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row select-tool" style="margin-bottom: 10px">
                    <label  class="col-sm-2" for="input-district">School District:</label>
                    <?php
                    if (strcmp($_SESSION['organization'], 'PED') === 0 || strcmp($_SESSION['organization'], 'SED') === 0) {
                        $str = <<<EOT
                    <div class='col-sm-3'>
                        <select id = "select-province" class = "form-control input-sm" onchange="fillDistrict()"></select>
                    </div>
                    <script type="text/javascript">
                        showSelectProvince();
                    </script>
EOT;
                        echo $str;
                    }
                    ?>
                    <div class="col-sm-3">
                        <select class="form-control input-sm" name="district-code" id="select-district-code">

                        </select>
                    </div>
                </div>
                <div class="row data-list" style="overflow-y: auto;">
                    <div id='loading' style="width: 100%; position: absolute; display: none;">
                        <div class='uil-ring-css' style='-webkit-transform:scale(0.99); margin: 0 auto; top: 30%'><div></div></div>
                    </div>
                    <table class="table table-bordered table-responsive">
                        <thead>
                            <tr class="danger">
                                <th>ID</th><th>District</th>
                                <th>Number Of School</th><th>Number Of DTMT</th>
                                <th>Period</th><th>Year/Quarter</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>