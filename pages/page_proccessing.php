<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$page = $_POST["page"];
if (!isset($_SESSION["username"]) && strcmp($page, 'login') !== 0) {
    http_response_code(401);
    die("Session time out!");
}
require './config.php';
$page = $_POST["page"];
$db = DB::getInstance();
DB::getInstance()->query("SET NAMES utf8"); // convert data from database to utf8
switch ($page) {
    case 'login':
        $username = $_POST["username"];
        $password = $_POST["password"];
        $db->query("select * from user where user_id=? and password=?", array($username, $password));
        if ($db->getRowCount() > 0) {
            $data = $db->getResults();
            $_SESSION["username"] = $data[0]["user_id"];
            $_SESSION["organization"] = $data[0]["organization"];
            $_SESSION["DTML"] = $data[0]["DTMT_member"];
            if (strcmp($_SESSION["DTML"], '1') === 0) {
                $_SESSION["DTML-district"] = $data[0]["DTMT_defult_district"];
                $dtmt_district = $data[0]["DTMT_defult_district"];
                if (strcmp($_SESSION["organization"], 'PED') === 0 || strcmp($_SESSION["organization"], 'SED') === 0) {
                    $sql = "select province_code from district where district_code = ? limit 1";
                    $db->query($sql, array($dtmt_district));
                    $_SESSION["province"] = $db->getResults()[0]["province_code"];
                } else if (strcmp($_SESSION["organization"], 'PoE') === 0 || strcmp($_SESSION["organization"], 'DoE') === 0) {
                    $_SESSION["province"] = $data[0]["province"];
                }
                $_SESSION["district"] = $dtmt_district;
            } else {
                if (strcmp($_SESSION["organization"], 'PED') === 0 || strcmp($_SESSION["organization"], 'SED') === 0) {
                    $sql = "select province_code from province limit 1";
                    $db->query($sql);
                    $_SESSION["province"] = $db->getResults()[0]["province_code"];
                    $sql = "select district_code from district where province_code=? limit 1";
                    $db->query($sql, array($_SESSION["province"]));
                    $_SESSION["district"] = $db->getResults()[0]["district_code"];
                } else if (strcmp($_SESSION["organization"], 'PoE') === 0) {
                    $sql = "select district_code from district where province_code=? limit 1";
                    $_SESSION["province"] = $data[0]["province"];
                    $db->query($sql, array($data[0]["province"]));
                    $_SESSION["district"] = $db->getResults()[0]["district_code"];
                } else if (strcmp($_SESSION["organization"], 'DoE') === 0) {
                    $_SESSION["province"] = $data[0]["province"];
                    $_SESSION["district"] = $data[0]["district"];
                }
                $_SESSION["DTML-district"] = $_SESSION["district"];
            }
            $_SESSION["enter-data"] = $data[0]["right_to_enter_data"];
            $_SESSION["edit-data"] = $data[0]["right_to_edit_data"];
            $_SESSION["access-statistic"] = $data[0]["right_to_access_statistic"];
            $_SESSION["school-distance"] = $data[0]["right_to_enter_school_distance"];
            $_SESSION["created-by"] = $data[0]["created_by"];
            $_SESSION["create-user"] = $data[0]["right_to_create_new_user"];
            $_SESSION["edit-user"] = $data[0]["right_to_edit_user_you_created"];
            http_response_code(200);
        } else {
            http_response_code(401);
        }
        break;

    case 'user':
        $action = $_POST["action"];
        switch ($action) {
            case 'view-profile':
                $sql = "SELECT * FROM user where user_id=?";
                $db->query($sql, array($_SESSION['username']));
                echo json_encode($db->getResults());
                break;
            case 'view':
//  get user detail 
                $sql = "";
                if (strcmp($_SESSION['created-by'], "system") === 0) {
                    $sql = "select * from user";
                } else {
                    $sql = "select * from user where created_by=?";
                }
                $db->query($sql, array($_SESSION['username']));
                echo json_encode($db->getResults());
                break;
            case 'register':
                if (strcmp($_SESSION["create-user"], '1') === 0) {
                    $data = $_POST['data'];
                    if (strcmp($data[3], 'SED') === 0 || strcmp($data[3], 'PED') === 0) {
                        $data[4] = '';
                        $data[5] = '';
                    } else if (strcmp($data[3], 'PoE') === 0) {
                        $data[5] = '';
                    }
                    if (strcmp($data[6], '0') === 0) { // dtmt member
                        $data[7] = '';
                    }
                    $sql = "INSERT INTO `user`(`full_name`, "
                            . "`user_id`,"
                            . "`password`,"
                            . "`organization`,"
                            . "`province`, "
                            . "`district`, "
                            . "`DTMT_member`, "
                            . "`DTMT_defult_district`, "
                            . "`created_by`,"
                            . "`right_to_enter_data`, "
                            . "`right_to_edit_data`,"
                            . "`right_to_access_statistic`, "
                            . "`right_to_enter_school_distance`, "
                            . "`right_to_create_new_user`, "
                            . "`right_to_edit_user_you_created`) "
                            . "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $db->dml($sql, $data);
                    if ($db->isError()) {
                        echo $db->getResponseText();
                        http_response_code(500);
                    }
                }
                break;
            case 'update':
                if (strcmp($_SESSION["edit-user"], "1") === 0) {
                    $data = $_POST['data'];
                    if (strcmp($data[2], 'SED') === 0 || strcmp($data[2], 'PED') === 0) {
                        $data[3] = '';
                        $data[4] = '';
                    } else if (strcmp($data[2], 'PoE') === 0) {
                        $data[4] = '';
                    } if (strcmp($data[5], '0') === 0) {
                        $data[6] = '';
                    }
                    if (strcmp($_SESSION["created-by"], "system") === 0) {
                        $sql = "UPDATE `user` SET `full_name`=?,"
                                . "`password`=?,"
                                . "`organization`=?,"
                                . "`province`=?,"
                                . "`district`=?,"
                                . "`DTMT_member`=?,"
                                . "`DTMT_defult_district`=?,"
                                . "`right_to_enter_data`=?,"
                                . "`right_to_edit_data`=?,"
                                . "`right_to_access_statistic`=?,"
                                . "`right_to_enter_school_distance`=?,"
                                . "`right_to_create_new_user`=?,"
                                . "`right_to_edit_user_you_created`=? "
                                . "WHERE user_id=?";
                    } else {
                        $sql = "UPDATE `user` SET `full_name`=?,"
                                . "`password`=?,"
                                . "`organization`=?,"
                                . "`province`=?,"
                                . "`district`=?,"
                                . "`DTMT_member`=?,"
                                . "`DTMT_defult_district`=?,"
                                . "`right_to_enter_data`=?,"
                                . "`right_to_edit_data`=?,"
                                . "`right_to_access_statistic`=?,"
                                . "`right_to_enter_school_distance`=?,"
                                . "`right_to_create_new_user`=?,"
                                . "`right_to_edit_user_you_created`=? "
                                . "WHERE user_id=? AND created_by=?";
                        $data[] = $_SESSION["username"];
                    }
                    $db->dml($sql, $data);
                    $db->query("select * from user where user_id='$data[13]'");
                    echo json_encode($db->getResults());
                    if ($db->isError()) {
                        echo $db->getResponseText();
                        http_response_code(500);
                    }
                }
                break;
            case 'delete':
                break;
            case 'check-id':
                $id = $_POST["id"];
                $db->query("select * from user where user_id=?", array($id));
                if ($db->getRowCount() > 0) {// existed
                    http_response_code(401);
                }
                break;
            default :break;
        }
        break;
    case 'school':
        $action = $_POST["action"];
        switch ($action) {
            case 'view':
//  get user detail
                $data = $_POST['search_string'];
                $school_cat = $_POST['school_cat'];
                $condition = '';
                if (strlen($school_cat) !== 0) {
                    switch ($school_cat) {
                        case 'all':
                            $condition = '';
                            break;
                        case 'lower-secondary':
                            $condition = 'has_lower_secondary = 1';
                            break;
                        case 'lower-secondary-primary':
                            $condition = 'has_lower_secondary = 1 and has_primary = 1';
                            break;
                        case 'primary':
                            $condition = 'has_primary = 1';
                            break;
                        default :
                            break;
                    }
                }
                if (strlen($condition) === 0) {
                    if (strlen($data) !== 0) {
                        $sql = "select school_code, school_name_khmer, school_name_latin, commune_name, village_name from school where district = " . $_SESSION["DTML-district"] . " and school_name_khmer like '%$data%' order by cast(school_code as unsigned)";
                    } else {
                        $sql = "select * from school where district = " . $_SESSION["DTML-district"] . " order by cast(school_code as unsigned)";
                    }
                } else {
                    if (strlen($data) !== 0) {
                        $sql = "select * from school where district = " . $_SESSION["DTML-district"] . " and $condition and school_name_khmer like '%$data%' order by cast(school_code as unsigned)";
                    } else {
                        $sql = "select * from school where district = " . $_SESSION["DTML-district"] . " and $condition order by cast(school_code as unsigned)";
                    }
                }
                $db->query($sql);
                echo json_encode($db->getResults());
                break;

            case 'add':
                if (strcmp($_SESSION["enter-data"], '1') === 0) {
                    $data = $_POST["data"];
                    $sql = "INSERT INTO `school`(`school_code`, `school_name_khmer`,"
                            . "`school_name_latin`, `province`, `district`, `commune`,"
                            . "`village`, `geolocation_x`, `geolocation_y`,"
                            . "`has_primary`, `has_lower_secondary`,"
                            . "`has_upper_secondary`, `has_vocational_training`,"
                            . "`last_visited_by_DTMT`, `distance_to_DoE_in_km`,"
                            . " `distance_to_DoE_in_minute`, `type_of_school`, "
                            . "`road_quality`, `when_accessible_by_road`)"
                            . " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $db->dml($sql, $data);
                    if ($db->isError()) {
                        echo $db->getResponseText();
                        http_response_code(500);
                    }
                }
                break;
            case 'update':
                if (strcmp($_SESSION["edit-data"], '1') === 0) {
                    $data = $_POST["data"];
                    $sql = "UPDATE `school` SET "
                            . "`distance_to_DoE_in_km`=?,"
                            . "`distance_to_DoE_in_minute`=?,"
                            . "`road_quality`=?,"
                            . "`when_accessible_by_road`=? "
                            . "WHERE school_code=?";
                    $db->dml($sql, $data);
                    $db->query("SELECT distance_to_DoE_in_km,distance_to_DoE_in_minute,"
                            . "road_quality,when_accessible_by_road from school where school_code=?", array($data[4]));
                    echo json_encode($db->getResults());
                    if ($db->isError()) {
                        echo $db->getResponseText();
                        http_response_code(500);
                    }
                }
                break;
            case 'delete':
                $school_list = $_POST["school_list"];
                $school_code = implode(",", $school_list);
                $sql = "DELETE FROM school WHERE school_code in ($school_code)";
                $db->dml($sql);
                if ($db->isError()) {
                    echo $db->getResponseText();
                    http_response_code(500);
                }
                break;
            default :break;
        }
        break;
    case 'planning':
        $action = $_POST["action"];
        switch ($action) {
            case 'select-district':
                if (strcmp($_SESSION['organization'], 'PED') === 0 || strcmp($_SESSION['organization'], 'SED') === 0) {
                    $sql = "SELECT district_code, district_name FROM district WHERE province_code=?";
                    $db->query($sql, array($_SESSION['province']));
                } else if (strcmp($_SESSION['organization'], 'PoE') === 0) {
                    $sql = "SELECT district_code, district_name FROM district WHERE province_code =?";
                    $db->query($sql, array($_SESSION['province']));
                } else {
                    $sql = "SELECT district_code, district_name FROM district WHERE district_code =?";
                    $db->query($sql, array($_SESSION['district']));
                }
                echo json_encode($db->getResults());
                break;
            case 'school-type':
                $district_code = $_POST["district_code"];
                $sql = "SELECT type_of_school,count(*) as number FROM `school` WHERE district =? GROUP BY type_of_school";
                $db->query($sql, array($district_code));
                $data = $db->getResults();
                $near = 0;
                $distant = 0;
                $remote = 0;
                for ($i = 0; $i < count($data); $i++) {
                    if (strcmp($data[$i]['type_of_school'], 'Near') === 0) {
                        $near = (int) $data[$i]['number'];
                    } else if (strcmp($data[$i]['type_of_school'], 'Distant') === 0) {
                        $distant = (int) $data[$i]['number'];
                    } else if (strcmp($data[$i]['type_of_school'], 'Remote') === 0) {
                        $remote = (int) $data[$i]['number'];
                    }
                }
                $number_of_school = (int) $_POST["number_of_school"];
                $total = $near + $distant + $remote;
                if (total === 0) {
                    echo 'No-Record';
                    http_response_code(500);
                    die();
                }
                if ($total < $number_of_school) {
                    echo "School found in database is less than number of school you input. There are $total schools found in database.";
                    http_response_code(500);
                    die();
                }
                $school['Near'] = 0;
                $school['Distant'] = 0;
                $school['Remote'] = 0;
                $region = array();
                $region[0]['float'] = ($near / $total) * $number_of_school;
                $region[1]['float'] = ($distant / $total) * $number_of_school;
                $region[2]['float'] = ($remote / $total) * $number_of_school;
//                echo $region[0]['float']."<br>";
//                echo $region[1]['float']."<br>";
//                echo $region[2]['float']."<br>";
                $region[0]['int'] = intval($region[0]['float']);
                $region[1]['int'] = intval($region[1]['float']);
                $region[2]['int'] = intval($region[2]['float']);
                $total_int = $region[0]['int'] + $region[1]['int'] + $region[2]['int'];
                if ($total_int < $number_of_school) {
                    $region[0]['float-less'] = $region[0]['float'] - $region[0]['int'];
                    $region[1]['float-less'] = $region[1]['float'] - $region[1]['int'];
                    $region[2]['float-less'] = $region[2]['float'] - $region[2]['int'];
                    $temp = $region;
                    $max1 = 0;
                    $max2 = 1;
                    for ($i = 0; $i < 2; $i++) {
                        for ($j = 1; $j < 3; $j++) {
                            if ($temp[$i]['float-less'] < $temp[$j]['float-less']) {
                                $tem = $temp[$i]['float-less'];
                                $temp[$i]['float-less'] = $temp[$j]['float-less'];
                                $temp[$j]['float-less'] = $tem;
                                if ($i === 0) {
                                    $max1 = $j;
                                }
                                if ($i === 1) {
                                    $max2 = $j;
                                }
                            }
                        }
                    }

                    if ($number_of_school - $total_int === 1) {
                        $region[$max1]['int'] ++;
                    }
                    if ($number_of_school - $total_int === 2) {
                        $region[$max1]['int'] ++;
                        $region[$max2]['int'] ++;
                    }
                }
                $school['Near'] = $region[0]['int'];
                $school['Distant'] = $region[1]['int'];
                $school['Remote'] = $region[2]['int'];
                echo json_encode($school);
                break;
            case 'get-school':
                $district_code = $_POST["district_code"];
                $cat = $_POST["cat"];
                $sql = "SELECT school_code, school_name_latin FROM school "
                        . "WHERE district = ? and type_of_school=? ORDER BY last_visited_by_DTMT";
                $db->query($sql, array($district_code, $cat));
                echo json_encode($db->getResults());
                break;
            case 'register-plan': // plan table and plan-school table are add;
                // plan table

                function isValidDate($input_date) {
                    $date_arr = explode('-', $input_date);
                    if (count($date_arr) === 3) {
                        if (checkdate($date_arr[1], $date_arr[0], $date_arr[2])) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }

                $district_code = $_POST['district_code'];
                $number_of_school = $_POST['number_of_school'];
                $number_of_dtmt = $_POST['number_of_dtmt'];
                $start_date = date("Y-m-d", strtotime($_POST['start_date']));
                $end_date = date("Y-m-d", strtotime($_POST['end_date']));
                $year = $_POST['year'];
                $quarter = $_POST['quarter'];
                if (strlen($district_code) === 0) {
                    http_response_code(500);
                    die("No district code detected!");
                }
                if ($number_of_school < 1 || strlen($number_of_school) === 0 || !is_numeric($number_of_school)) {
                    http_response_code(500);
                    die("No number of school is detected!");
                }
                if ($number_of_dtmt < 1 || strlen($number_of_dtmt) === 0 || !is_numeric($number_of_dtmt)) {
                    http_response_code(500);
                    die("No number of DTMT is detected!");
                }
                if (!isValidDate($_POST['start_date'])) {
                    http_response_code(500);
                    die("Input start date is incorrect!");
                }
                if (!isValidDate($_POST['end_date'])) {
                    http_response_code(500);
                    die("Input end date is incorrect!");
                }
                if (strlen($year) !== 4 || !is_numeric($year)) {
                    http_response_code(500);
                    die("Input year is incorrect!");
                }
                if (strlen($quarter) === 0 || ($quarter < 0 && $quarter > 4)) {
                    http_response_code(500);
                    die("Incorrect quarter!");
                }
                //-----------------------------
                // plan school
                $db->beginTransaction();
                $sql = "INSERT INTO `plan`("
                        . "`district_code`, "
                        . "`number_of_school`, "
                        . "`number_of_dtmt`, "
                        . "`year`, "
                        . "`quarter_number`, "
                        . "`start_of_date`, "
                        . "`end_of_date`, "
                        . "`prepared_by`, "
                        . "`approved_by`) "
                        . "VALUES (?,?,?,?,?,?,?,?,?)";
                $db->dml($sql, array($district_code, $number_of_school, $number_of_dtmt, $year, $quarter, $start_date, $end_date, $_SESSION['username'], 'test'));
                if ($db->isError()) {
                    $db->rollBack();
                    http_response_code(500);
                    die($db->getResponseText());
                }
                $plan_id = $db->getLastInsertId();
                $school_code = $_POST['school_code'];
                $mission_date = $_POST["mission_date"];
                $shift = $_POST['shift'];
                $type = $_POST['type'];
                for ($i = 0; $i < $number_of_school; $i++) {
                    if (!isValidDate($mission_date[$i])) {
                        http_response_code(500);
                        die("Please check your mission date");
                    }
                    $m_date = date("Y-m-d", strtotime($mission_date[$i]));
                    $sql = "INSERT INTO `plan_school`(`plan_id`, "
                            . "`school_code`, "
                            . "`mission_date`, "
                            . "`shift`, "
                            . "`type_of_school`) "
                            . "VALUES (?,?,?,?,?)";
                    $db->dml($sql, array($plan_id, $school_code[$i], $m_date, $shift[$i], $type[$i]));
                    if ($db->isError()) {
                        $db->rollBack();
                        http_response_code(500);
                        die($db->getResponseText());
                    }
                }
                $db->commit();
        }
        break;
    default :break;
}
