<?php
require '../template/header.php';
?>
<style>
    .panel{
        position: fixed;
        width: 30%;
        top: 50%;
        left: 50%;
        -ms-transform: translate(-50%,-70%);
        -webkit-transform: translate(-50%, -70%);
        transform: translate(-50%, -70%);

    }

</style>
<script>
    var login = function () {

        var username = $("#username").val();
        var password = $("#password").val();
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            async: false,
            data: {page: "login", username: username, password: password},
            success: function () {
                // Redirect to home page
                var page = "http://localhost/child-friendly-school";
                window.location = page;
            },
            error: function () {
                $("#error").show();
            }
        });
    };
    $(document).ready(function () {
        $("#login").click(function () {
            login();
        });
        $("input").keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
                login();
            }
        });
    });
</script>
<body>
    <div class="container">
        <div class="col-sm-5 col-sm-offset-3" style="vertical-align: middle;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <label id="error" style="color:red; display: none;">Incorrect username or Password</label>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Username:</span>
                            <input id="username" class="form-control" type="text" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Password: </span>
                            <input id="password" class="form-control" type="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <button id="login" class="btn btn-primary pull-right">Login</button>
                    </div>
                </div>
            </div> 
        </div>

    </div>

</body>
