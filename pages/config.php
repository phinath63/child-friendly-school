<?php
session_start();
$home_page = "localhost/child-friendly-school/";
$GLOBALS["config"] = array(
    "mssqlserver" => array(
        "host" => "IT-PC",
        "username" => "sa",
        "password" => "phinath",
        "db" => "IPRHR"
    ),
    "remember" => array(
        "cookie_name" => "hash",
        "cookie_expiry" => 604800
    ),
    "session" => array(
        "session_name" => "user_name",
        "session_usertype" => "user_type",
        "session_userid" => 'user_id',
        "token_name" => "token"
    ),
);
spl_autoload_register(function($class) {
    require_once '../classes/' . $class . ".php";
});


