<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<style>
    td{
        font-weight: bold;
        font-size: 1.2em;
        color:black;
    }
    .table>tbody>tr>td{
        width: 20%;
    }
</style>
<script>
    function loadData(action) {
        $("#loading").show();
        $.ajax({
            url: "page_proccessing.php",
            type: "POST",
            async: false,
            data: {page: "user", action: action},
            success: function (result) {
                // Redirect to home page
                var data = JSON.parse(result);
                $("#data-list").html();
                var string;
                string = "<tr>" +
                        "<td>Full Name</td>" +
                        "<td>:" + data[0].full_name + "</td>" +
                        "</tr><tr>" +
                        "<td>User ID</td>" +
                        "<td>:" + data[0].user_id + "</td >" +
                        "</tr><tr>" +
                        "<td>Organization</td>" +
                        "<td>:" + data[0].organization + "</td>" +
                        "</tr><tr>" +
                        "<td>Province Code</td>" +
                        "<td>:" + (data[0].province === '0' ? '' : data[0].province) + "</td>" +
                        "</tr><tr>" +
                        "<td>District Code</td>" +
                        "<td>:" + (data[0].district === '0' ? '' : data[0].district) + "</td>" +
                        "</tr><tr>" +
                        "<td>DTMT Member</td>" +
                        "<td>:" + data[0].DTMT_member + "</span></td>" +
                        "</tr><tr>" +
                        "<td>DTMT Type</td>" +
                        "<td>:" + (data[0].DTMT_member === '0' ? '' : data[0].DTMT_defult_district) + "</td>" +
                        "</tr><tr>" +
                        "<td>Created By</td>" +
                        "<td>:" + data[0].created_by + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Enter Data</td>" +
                        "<td>:" + data[0].right_to_enter_data + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Edit Data</td>" +
                        "<td>:" + data[0].right_to_edit_data + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Access Statistic</td>" +
                        "<td>:" + data[0].right_to_access_statistic + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Enter School Distance</td>" +
                        "<td>:" + data[0].right_to_enter_school_distance + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Create New User</td>" +
                        "<td>:" + data[0].right_to_create_new_user + "</td>" +
                        "</tr><tr>" +
                        "<td>Right To Edit User</td>" +
                        "<td>:" + data[0].right_to_edit_user_you_created + "</td>" +
                        "</tr>";
                $("#data-list").append(string);
            },
            error: function () {
                $("#error").show();
            },
            complete: function () {
                $("#loading").hide();
            }
        });
    }
    $(document).ready(function () {
        loadData('view-profile')
    });
</script>
<div class="col-sm-10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 photo">
                        <a href="#" class="thumbnail">
                            <img src="<?php echo "media/picture/" . $_SESSION["username"] . ".jpg"; ?>" alt="...">
                        </a>
                    </div>
                    <div class="col-sm-9">
                        <table class="table table-hover table-striped">
                            <thead></thead>
                            <tbody id="data-list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>       
</div>
