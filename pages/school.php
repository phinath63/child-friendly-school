<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<style>
</style>
<script src="js/common.js">
</script>
<script src="js/school.js">
</script>
<span id="organization" class="hidden"><?php echo $_SESSION["organization"]; ?></span>
<span id="default-province" class="hidden"><?php echo $_SESSION["province"]; ?></span>
<span id="default-district" class="hidden"><?php echo $_SESSION["district"]; ?></span>
<span id="default-dtmt" class="hidden"><?php echo $_SESSION["DTML-district"]; ?></span>
<span id="right-to-enter-data" class="hidden"><?php echo $_SESSION["enter-data"]; ?></span>
<span id="right-to-edit-data" class="hidden"><?php echo $_SESSION["edit-data"]; ?></span>
<span id="right-to-access-statistic" class="hidden"><?php echo $_SESSION["access-statistic"]; ?></span>
<span id="right-to-enter-school-distance" class="hidden"><?php echo $_SESSION["school-distance"]; ?></span>
<span id="right-to-create-new-user" class="hidden"><?php echo $_SESSION["create-user"]; ?></span>
<span id="right-to-create-edit-user" class="hidden"><?php echo $_SESSION["edit-user"]; ?></span>

<div class="col-sm-10 content">
    <div class="panel panel-default">
        <div class="panel-body" style="padding-right: 0;">
            <div class="container-fluid">
                <div class="row select-tool" style="margin-bottom: 10px;">
                    <div class="col-sm-9" style="padding: 0;">
                        <?php
                        $str = '';
                        if (strcmp($_SESSION["organization"], "PED") === 0 || strcmp($_SESSION["organization"], "SED") === 0) {
                            $str = <<<EOD
                <div class="col-sm-4" style="padding:0;">
                    <div>
                        <select id="select-school-cat" class="form-control" onchange="loadData('view');">
                            <option value="all">All</option>
                            <option value="lower-secondary">Lower Secondary</option>        
                            <option value="lower-secondary-primary">Primary & Lower Secondary</option>
                            <option value="primary">Primary</option>
                        </select>
                    </div>
                </div>
                <div class = "col-sm-4">
                    <div>
                        <select id = "select-province" class = "form-control" onchange="sessionOperation('province','change-dtmt');"></select>
                    </div>
                    <script type="text/javascript">
                        showSelectProvince();
                    </script>
                </div>
                <div class = "col-sm-4">
                    <div>
                        <select id = "select-district" class = "form-control" onchange="sessionOperation('district','change-dtmt');"></select>
                    </div>
                    <script type="text/javascript">
                        showSelectDistrict();
                    </script>
                </div>
EOD;
                        } else if (strcmp($_SESSION["organization"], "PoE") === 0) {
                            $str = <<<EOD
                <div class = "col-sm-4">
                    <div>
                        <select id = "select-district" class = "form-control" onchange="sessionOperation('district','change-dtmt');"></select>
                    </div>
                    <script type="text/javascript">
                        showSelectDistrict();
                    </script>
                </div>
EOD;
                        }
                        echo $str;
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <input id="search-input" type="text" class="form-control" placeholder="search by name in khmer" oninput="search();">
                        </div>
                    </div>
                </div>
                <div class="row data-list" style="overflow-y: auto">
                    <div id='loading' style="width: 100%; position: absolute; display: none;">
                        <div class='uil-ring-css' style='-webkit-transform:scale(0.99); margin: 0 auto; top: 30%'><div></div></div>
                    </div>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="danger">
                                <th><input type="checkbox" id="check-all">Code</th><th>Name</th><th>Latin</th>
                                <th>Commune</th><th>Village</th><th>Distance to DoE (km)</th>
                                <th>Distance to DOE (min)</th><th>Road</th>
                                <th>Road Detail</th>
                                <?php
                                if (strcmp($_SESSION["edit-data"], '1') === 0) {
                                    echo "<th>Action</th>";
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody id="data-list">

                        </tbody>
                    </table>
                    <div class="col-sm-3 btn-wrapper">
<!--                            <span style="margin-left: 15px;">Select All</span>
                        <div class="btn btn-sm btn-success" style="height: 30px;"><input type="checkbox" id="check-all"></div>-->
                        <button id="delete" class="btn btn-danger btn-sm" onclick='deleteSchool()' style="display: none;"><i class="fa fa-trash"></i>Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (strcmp($_SESSION["edit-data"], "1") === 0) {
    ?>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">School</h4>
                </div>
                <div class="modal-body">
                    <div class="level-operation form-horizontal">
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-code">School Code:</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="code" id="txt-code" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-name">School Name(Khmer):</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="name" id="txt-name" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-name-latin">School Name(Latin):</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="name" id="txt-name-latin" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-commune">Commune:</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="commune-code" id="txt-commune-code" disabled>
    <!--                                <select class="form-control input-sm" name="commune-code" id="select-commune-code" onchange="loadNew('commune')"></select>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-village">Village:</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="village-code" id="txt-village-code" disabled>
    <!--                                <select class="form-control input-sm" name="vilage-code" id="select-village-code" onchange="loadNew('village')"></select>-->
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-5" for="school-distince-DoE">Distance to DoE (KM):</label>
                            <div class="col-sm-7">
                                <?php
                                if (strcmp($_SESSION["school-distance"], '1') === 0) {
                                    echo "<input class='form-control input-sm' type='text' name='distance-to-DoE(km)' id='txt-distance-to-DoE-km'>";
                                } else {
                                    echo "<input disabled class='form-control input-sm' type='text' name='distance-to-DoE(km)' id='txt-distance-to-DoE-km'>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-distince-DoE">Distance to DoE (min):</label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" name="distance-to-DoE(mn)" id="txt-distance-to-DoE-mn">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-road-quality">Road Quality:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" name="road-quality" id="select-road-quality">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="school-when-accessible-by-road">When Accessible:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" name="when-accessible-by-road" id="select-when-accessible">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" name = "save" onclick='update()' id="btn-save-update">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>


</div>
</body>
</html>
