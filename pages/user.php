<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<style>
    .red{
        color:red;
    }
</style>
<script src="js/common.js">
</script>
<script src="js/user.js">
</script>
<span id="right-to-edit" class="hidden"><?php echo $_SESSION["edit-user"]; ?></span>
<div class="col-sm-10 data-list" style="overflow-y: auto;">
    <div id='loading' style="width: 100%; position: absolute; display: none;">
        <div class='uil-ring-css' style='-webkit-transform:scale(0.99); margin: 0 auto; top: 30%'><div></div></div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr class="info">
                                    <th>Full Name</th><th>Use Id</th><th>Organization</th><th>Province</th>
                                    <th>District</th><th>DTMT</th>
                                    <th>DTMT Type</th><th>Created By</th><th>Enter Data</th>
                                    <th>Edit Data</th><th>Access Statistic</th>
                                    <th>Enter School Distance</th><th>Create New User </th>
                                    <th>Edit User</th>
                                    <?php
                                    if (strcmp($_SESSION["edit-user"], '1') === 0) {
                                        echo "<th>Action</th>";
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody id="data-list">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
if (strcmp($_SESSION["edit-user"], "1") === 0) {
    ?>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">User</h4>
                </div>
                <div class="modal-body">
                    <div class="level-operation form-horizontal">
                        <div class="form-group">
                            <label  class="col-sm-5" for="full-name">Full Name:<span id='lb-full-name' class='red pull-right'></span></label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" placeholder="Enter Full Name" name="full-name" id="txt-full-name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="user-id">User Id:<span id='lb-user-id' class='red pull-right'></span><img id="img-user-id" class="pull-right" src="media/picture/checking_loader.gif" style="display: none"></label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="text" placeholder="Enter User Id" name="user-id" id="txt-user-id" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="password">Password:<span id='lb-password' class='red pull-right'></span></label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="password" placeholder="Enter Password" name="password" id="txt-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="password">Confirm Password:<span id='lb-con-password' class='red pull-right'></span></label>
                            <div class="col-sm-7">
                                <input class="form-control input-sm" type="password" placeholder="Enter Password Again" name="con-password" id="txt-con-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="organization">Organization:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="organization" id="select-organization" onchange="checkOrganization()">
                                </select>
                            </div>
                        </div>
                        <div id='province' class="form-group">
                            <label  class="col-sm-5" for="province">Province:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="province" id="select-province" onchange="fillLocation('district')"></select>
                            </div>
                        </div>
                        <div id='district' class="form-group">
                            <label  class="col-sm-5" for="district">District:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="district" id="select-district" onchange="fillLocation('dtmt-type')"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="DTMT-member">DTMT Member:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="dtmt-member" id="select-dtmt-member" onchange="isDTMT()">
                                </select>
                            </div>
                        </div>
                        <div id='dtmt-type' class="form-group" style="display: none">
                            <label  class="col-sm-5" for="DTMT-type">DTML Type:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="dtmt-type" id="select-dtmt-district">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-enter-data">Right to Enter Data:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="enter-data" id="select-right-to-enter-data">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-edit-data">Right to Edit Data:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="edit-data" id="select-right-to-edit-data">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-access-statistic">Right to Access Statistic:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="edit-data" id="select-right-to-access-statistic">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-enter-school-distance">Right to Enter School Distance:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="right-to-enter-school-distance" id="select-right-to-enter-school-distance">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-create-new-user">Right to Create New User:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="right-to-create-new-user" id="select-right-to-create-new-user">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-5" for="right-to-edit-user">Right to Edit User:</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm" type="text" name="right-to-edit-user" id="select-right-to-edit-user">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" name = "save" onclick='update()' id="btn-save-update">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

</body>
</html>
