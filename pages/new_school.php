<?php
require '../template/header.php';
require '../template/sidebar.php';
?>
<script src="js/common.js">
</script>
<script src="js/new_school.js">
</script>
<div class="col-sm-10">
    <div class="container-fluid">
        <div class="level-operation form-horizontal" style="border: 1px solid blueviolet;padding: 20px;">
            <div class="form-group">
                <label  class="col-sm-5" for="school-code">School Code:<span id='lb-school-code' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="code" id="txt-code">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-name">School Name(Khmer):<span id='lb-school-name-khmer' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="name" id="txt-name">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-name-latin">School Name(Latin):<span id='lb-school-name-latin' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="name" id="txt-name-latin">
                </div>
            </div>

            <div class="form-group">
                <label  class="col-sm-5" for="school-province">Province:<span id='lb-school-province' class='red pull-right'></span></label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="province-code" id="select-province-code" onchange="loadNew('province')"></select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-district">District Code:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="district-code" id="select-district-code" onchange="loadNew('district')"></select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-commune">Commune Code:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="commune-code" id="select-commune-code" onchange="loadNew('commune')"></select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-village">Village Code:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="vilage-code" id="select-village-code"></select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-geolocation-x">Long:</label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="long" id="txt-long">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-geolocation-y">Lat:</label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="code" id="txt-lat">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-has-primary">Has Primary:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="has-primary" id="select-has-primary">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-has-lower-secondary">Has Lower Secondary:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="has-lower-secondary" id="select-has-lower-secondary">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-has-upper-secondary">Has Upper Secondary:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="has-upper-secondary" id="select-has-upper-secondary">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-has-vocational">Has Vocational:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="has-vocational" id="select-has-vocational">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-last-visited-by-DTMT">DTMT last visited:</label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="date" name="last-visit" id="txt-last-visited-DTMT">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-distince-DoE">Distance to DoE(KM):</label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="distance-to-DoE(km)" id="txt-distance-to-DoE-km">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-distince-DoE">Distance to DoE(mn):</label>
                <div class="col-sm-7">
                    <input class="form-control input-sm" type="text" name="distance-to-DoE(mn)" id="txt-distance-to-DoE-mn">
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-type">Type of School:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="type-of-shcool" id="select-school-type">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-road-quality">Road Quality:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="road-quality" id="select-road-quality">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-5" for="school-when-accessible-by-road">When Accessible:</label>
                <div class="col-sm-7">
                    <select class="form-control input-sm" name="when-accessible-by-road" id="select-when-accessible">
                    </select>
                </div>
            </div>
            <div class="row" style="padding-right: 20px;">
                <button id='register' class="btn btn-primary pull-right">Register</button>
            </div>
        </div>
    </div>
</div>
