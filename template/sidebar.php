<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION["username"])) {
    header("Location: $home/pages/login.php");
}
?>
<style>
    th, td{
        text-align: center;
    }
</style>
<script>
    var toggleNav = function (id) {
        $("#" + id).animate({
            height: 'toggle'
        });
    };
    var setHeight = function () {
        var viewPort = $(window).height();
        var navBar = $(".nav-bar").height();
        $(".content").height(viewPort - navBar - 22);
        var select_tool = $(".select-tool").height();
        $(".data-list").height(viewPort - navBar - select_tool - 85);
        $("#loading").height(viewPort - navBar - select_tool - 100);
    };
    $(window).resize(function () {
        setHeight();
    });
</script>
<body>
    <nav class="navbar navbar-default nav-bar" role="navigation">
        <div class="container-fluid">
            <div class="page-logo"><a class="navbar-brand" href="#">Child Friendly School</a></div>
            <div class="top_menu">
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                            <span class="glyphicon glyphicon-user">&nbsp;</span>Signed in as <?php echo $_SESSION["username"]; ?> &nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="profile.php"><span class="glyphicon glyphicon-plus">&nbsp;</span>View Profile</a></li>
                            <li role="presentation" class="divider"></li>
                            <li><a href="logout.php"><span class="glyphicon glyphicon-log-out">&nbsp;</span>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <span id='message' style="color:green;font-weight: bold;"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="timeout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <span id='text' style="color:red;font-weight: bold;">Session time out!</span><br>
                    <a href="<?php echo "$home/pages/login.php"?>">Click here to login again!</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid main-container">
        <div class="navigation col-sm-2">
            <div class="panel panel-default">
                <div class="panel-body" style="padding: 0;">
                    <ul class="nav main-menu nav-pills nav-stacked">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li><a href="#" id="administrator-group" onclick="toggleNav('administrator')"><i class="fa fa-users"></i> Administrator<i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="nav nav-pills nav-stacked col-sm-offset-1" id="administrator" style="display: none">
                                <li><a  href="user.php"><i class="fa fa-user"></i>Users</a></li>
                                <li><a  href="user_registration.php"><i class="fa fa-users"></i>New User</a></li>
                            </ul>
                        </li>	
                        <li><a href="#" id="school-menu" onclick="toggleNav('school')"><i class="fa fa-home"></i> Manage Schools<i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="nav nav-pills nav-stacked col-sm-offset-1" id="school" style="display: none">
                                <li><a href="school.php"><i class="fa fa-user"></i>Schools</a></li>
                                <li><a  href="new_school.php"><i class="fa fa-user-plus"></i>New School</a></li>
                            </ul>
                        </li>
                        <li><a id="plan-menu" href="planning.php"><i class="fa fa-flag-checkered"></i> Do Planning</a></li>
                        <li><a id="school-plan-menu" href="view_planning.php"><i class="fa fa-level-up"></i> View Planning</a></li>
                        <li><a id="school-visit-menu" href="import_school.php"><i class="fa fa-level-up"></i> Import School</a></li>
                        <li><a id="school-visit-menu" href="#"><i class="fa fa-bookmark-o"></i> Report</a></li>
                    </ul>
                </div>
            </div>
        </div>
