<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.1">
        <meta content="noindex,nofollow" name="robots">
        <meta content="IE=Edge" http-equiv="X-UA-Compatible">
        <title>Child Friendly School</title>


        <!-- Bootstrap, Tab, Normailze CSS -->
        <link rel="stylesheet" href="../libs/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/style.css">

        <script src="../libs/js/jquery-2.1.1.min.js"></script>
        <script src="../libs/js/bootstrap.min.js"></script>

    </head>
    <?php
        include "core.php";
    ?>

