<?php

class DB {

    private static $_instance = null;
    private $_pdo,
            $_query,
            $_error = false,
            $_results,
            $_count = 0,
            $_message = '';

    private function __construct() {
        try {
            $server = '192.168.0.3';
            $database = 'Abacus_002';
            $username = 'sa';
            $password = 'admin@IPR_999';

            //$this->_pdo = new PDO("mysql:Server=$server;Database=$database", $username, $password);
            $this->_pdo = new PDO("odbc:Driver={SQL Server};Server=$server;database=$database", $username, $password);
            //$this->_pdo = new PDO('sqlsrv:Server=' . Config::get('mssqlserver/host') . ';Database=' . Config::get('mssqlserver/db'), Config::get('mssqlserver/username'), Config::get('mssqlserver/password'), array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            // $this->_pdo = new PDO('sqlsrv:Server=IT-PC;Database=IPRHR', 'sa', 'phinath');
            //$this->_pdo = new PDO("odbc:Driver={SQL Server};Server=IT-PC;Database=IPRHR;Uid=sa;Pwd=phinath");
        } catch (PDOException $exc) {
            die($exc->getMessage());
        }
    }

    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    public function beginTransaction() {
        $this->_pdo->beginTransaction();
    }

    public function commit() {
        $this->_pdo->commit();
    }

    public function rollBack() {
        $this->_pdo->rollBack();
    }

    public function getRowCount() {
        return $this->_count;
    }

    public function getResults() {
        return $this->_results;
    }

    public function query($sql, $params = array()) {
        try {
            $this->_query = $this->_pdo->prepare($sql);
            $result = $this->_query->execute($params);
            $this->_error = false;
            if ($result) {
                $this->_results = $this->_query->fetchAll(PDO::FETCH_ASSOC);
                $this->_count = $this->_query->rowCount();
            }
        } catch (Exception $ex) {
            $this->_error = true;
        }
    }

    public function dml($sql, $params = array()) {
        try {
            $this->_query = $this->_pdo->prepare($sql);
            $this->_query->execute($params);
            $this->_error = false;
        } catch (Exception $ex) {
            $this->_error = true;
            $this->_message = $ex->getMessage();
        }
    }

    public function isError() {
        return $this->_error;
    }

    public function getResponseText() {
        return $this->_message;
    }

    public function getLastInsertId() {
        return $this->_pdo->lastInsertId();
    }

}
